#!/bin/bash

# any future command that fails will exit the script
set -e

# Delete the old repo
sudo rm -rf /home/ubuntu/cicd_politician_frontend/

# clone the repo again
sudo git clone https://gitlab.com/krupalsharmakn/cicd_politician_frontend.git

#source the nvm file. In an non
#If you are not using nvm, add the actual path like
# PATH=/home/ubuntu/node/bin:$PATH
#source /home/ubuntu/.nvm/nvm.sh

# stop the previous pm2
#sudo pm2 kill
#sudo npm remove pm2 -g


#pm2 needs to be installed globally as we would be deleting the repo folder.
# this needs to be done only once as a setup script.
#sudo npm install pm2 -g
# starting pm2 daemon
#sudo pm2 status
sudo chmod -R 777 /home/ubuntu/cicd_politician_frontend
cd /home/ubuntu/cicd_politician_frontend

#install npm packages
echo "Running npm install"
#sudo npm install 

#Restart the node server
#sudo npm start
sudo service nginx start
#sudo pm2 start npm --name "politician_server_9004" --kill-timeout 3000 -- start

