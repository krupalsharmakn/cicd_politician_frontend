(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./layout/layout.module": [
		"./src/app/layout/layout.module.ts",
		"layout-layout-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/guard/auth.guard */ "./src/app/shared/guard/auth.guard.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _layout_privacy_privacy_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./layout/privacy/privacy.component */ "./src/app/layout/privacy/privacy.component.ts");







var routes = [
    {
        path: '',
        loadChildren: './layout/layout.module#LayoutModule',
        canActivate: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_4__["LoginComponent"]
    },
    {
        path: 'signup',
        component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_5__["SignupComponent"]
    },
    {
        path: 'privacy_policy',
        component: _layout_privacy_privacy_component__WEBPACK_IMPORTED_MODULE_6__["PrivacyComponent"]
    },
    {
        path: '**',
        redirectTo: 'not-found'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
            providers: [_shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_3__["AuthGuard"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/esm5/icon.es5.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");





var AppComponent = /** @class */ (function () {
    function AppComponent(translate, domSanitizer, matIconRegistry) {
        this.translate = translate;
        this.domSanitizer = domSanitizer;
        this.matIconRegistry = matIconRegistry;
        translate.setDefaultLang('en');
        this.matIconRegistry.addSvgIcon('meeting', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/business-meeting-svgrepo-com.svg'));
        this.matIconRegistry.addSvgIcon('achievement', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/achievement.svg'));
        this.matIconRegistry.addSvgIcon('complaints', this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/complaints.svg'));
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["DomSanitizer"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__["MatIconRegistry"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: createTranslateLoader, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTranslateLoader", function() { return createTranslateLoader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm5/ngx-translate-http-loader.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_api_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/services/api-service */ "./src/app/shared/services/api-service.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/signup/signup.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _layout_privacy_privacy_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./layout/privacy/privacy.component */ "./src/app/layout/privacy/privacy.component.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/index.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/index.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./shared/guard/auth.guard */ "./src/app/shared/guard/auth.guard.ts");





















//import { FirebaseService } from './services/firebase.service';





// // AoT requires an exported function for factories
var createTranslateLoader = function (http) {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-javascript/sb-admin-material/master/dist/assets/i18n/',
        '.json'
    );*/
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__["TranslateHttpLoader"](http, './assets/i18n/', '.json');
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"], _signup_signup_component__WEBPACK_IMPORTED_MODULE_15__["SignupComponent"], _login_login_component__WEBPACK_IMPORTED_MODULE_16__["LoginComponent"], _layout_privacy_privacy_component__WEBPACK_IMPORTED_MODULE_17__["PrivacyComponent"]],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_11__["AppRoutingModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_1__["LayoutModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_2__["OverlayModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_13__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_13__["ReactiveFormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_9__["CommonModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_10__["HttpModule"],
                _angular_fire__WEBPACK_IMPORTED_MODULE_18__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_20__["environment"].firebaseConfig),
                _angular_fire_database__WEBPACK_IMPORTED_MODULE_21__["AngularFireDatabaseModule"],
                _angular_fire_auth__WEBPACK_IMPORTED_MODULE_22__["AngularFireAuthModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_23__["FlexLayoutModule"],
                _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_19__["AngularFirestoreModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_24__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_24__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_24__["MatDialogModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
                        useFactory: createTranslateLoader,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]]
                    }
                })
            ],
            providers: [_shared_services_api_service__WEBPACK_IMPORTED_MODULE_14__["ApiService"], _shared_guard_auth_guard__WEBPACK_IMPORTED_MODULE_25__["AuthGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_12__["AppComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_3__["CUSTOM_ELEMENTS_SCHEMA"], _angular_core__WEBPACK_IMPORTED_MODULE_3__["NO_ERRORS_SCHEMA"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/layout/privacy/privacy.component.css":
/*!******************************************************!*\
  !*** ./src/app/layout/privacy/privacy.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "p {\n    text-align: justify;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGF5b3V0L3ByaXZhY3kvcHJpdmFjeS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1CO0FBQ3ZCIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L3ByaXZhY3kvcHJpdmFjeS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsicCB7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/layout/privacy/privacy.component.html":
/*!*******************************************************!*\
  !*** ./src/app/layout/privacy/privacy.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n  \n  <div>\n    <div class=\"container my-5\">\n      <h2>\n        Privacy & policy\n      </h2>\n      <p>\n        This page informs you of our policies regarding the collection, use, and disclosure of personal data when you use\n        our Service and the choices you have associated with that data. .\n      </p>\n      <p>We use your data to provide and improve the Service. By using the Service, you agree to the collection and use of\n        information in accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this\n        Privacy Policy have the same meanings as in our Terms and Conditions.\n      </p>\n      <p>Information Collection And Use</p>\n      <p>\n        We collect several different types of information for various purposes to provide and improve our Service to you.\n      </p>\n      <p>Types of Data Collected<br>Personal Data</p>\n      <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can\n        be used to contact or identify you (\"Personal Data\"). Personally, identifiable information may include, but is not\n        limited to:</p>\n        <p>First name and last name<br>Phone number<br>Cookies and Usage Data<br>Usage Data</p>\n  <p>When you access the Service by or through a mobile device, we may collect certain information automatically, including, but not limited to, the type of mobile device you use, your mobile device unique ID, the IP address of your mobile device, your mobile operating system, the type of mobile Internet browser you use, unique device identifiers and other diagnostic data (\"Usage Data\").</p>\n  <p>Tracking & Cookies Data</p>\n  <p>We use cookies and similar tracking technologies to track the activity on our Service and hold certain information.</p>\n  <p>Cookies are files with a small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyze our Service.</p>\n  <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.</p>\n  <p>Examples of Cookies we use:</p>\n  <p>Session Cookies. We use Session Cookies to operate our Service.<br>Preference Cookies. We use Preference Cookies to remember your preferences and various settings.</p>\n  <p>Security Cookies. We use Security Cookies for security purposes.<br>Use of Data<br>MRV uses the collected data for various purposes:</p>\n  <p>To provide and maintain the Service<br>To notify you about changes to our Service<br>To allow you to participate in interactive features of our Service when you choose to do so</p>\n  <p>To provide customer care and support<br>To provide analysis or valuable information so that we can improve the Service<br>To monitor the usage of the Service<br>To detect, prevent and address technical issues<br>Transfer Of Data</p>\n  <p>Your information, including Personal Data, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>\n  <p>If you are located outside India and choose to provide information to us, please note that we transfer the data, including Personal Data, to India and process it there.</p>\n  <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>\n  <p>MRV will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>\n  <p>Disclosure Of Data <br>Legal Requirements</p>\n  <p>OPR may disclose your Personal Data in the good faith belief that such action is necessary to:\n   </p>\n  <p>To comply with a legal obligation<br>\n  To protect and defend the rights or property of MRV<br>\n  To prevent or investigate possible wrongdoing in connection with the Service<br>\n  To protect the personal safety of users of the Service or the public<br>\n  To protect against legal liability</p>\n  <p>Security Of Data<br>\n  The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>\n  <p>Service Providers<br>\n  We may employ third party companies and individuals to facilitate our Service (\"Service Providers\"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>  \n  <p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.\n   </p>\n  <p>\n    Links To Other Sites<br>\n  Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.\n  </p>\n  <p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>\n  <p>Children's Privacy<br>\n  Our Service does not address anyone under the age of 18 (\"Children\").</p>\n  <p>We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children has provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from children without verification of parental consent, we take steps to remove that information from our servers.</p>\n  <p>Changes To This Privacy Policy<br>\n  We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.\n   </p>\n  <p>We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the \"effective date\" at the top of this Privacy Policy.</p>\n  <p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>\n  <p>Contact Us<br>\n  If you have any questions about this Privacy Policy, please contact us:<br>\n  contact@avohi.com</p>\n  </div>\n  \n  </div>"

/***/ }),

/***/ "./src/app/layout/privacy/privacy.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/layout/privacy/privacy.component.ts ***!
  \*****************************************************/
/*! exports provided: PrivacyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivacyComponent", function() { return PrivacyComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PrivacyComponent = /** @class */ (function () {
    function PrivacyComponent() {
    }
    PrivacyComponent.prototype.ngOnInit = function () {
    };
    PrivacyComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-privacy',
            template: __webpack_require__(/*! ./privacy.component.html */ "./src/app/layout/privacy/privacy.component.html"),
            styles: [__webpack_require__(/*! ./privacy.component.css */ "./src/app/layout/privacy/privacy.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PrivacyComponent);
    return PrivacyComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid h-100\">\n    <div class=\"row h-100\" >\n        <div class=\"col-md-8 text-center \" style=\"background:#278d27;\">\n            <div class=\"profile_image\">\n            <img  class=\"mla_profile\" src=\"assets/images/Yeddyurappa.jpg\">\n            <h2> <font color=\"white\">Mr.B.S.Yeddyurappa</font>  </h2>\n            <h6><font color=\"white\">Chief minister of Karnataka.<br></font></h6>\n            </div>   \n        </div>\n        <div class=\"col-md-4 text-center\" >\n            <div class=\"b-padding\">\n                <div class=\"text-center\">\n                    <h2 class=\"app-name \">Personal Diary</h2>\n                </div>\n                <form [formGroup]=\"loginform\" class=\"login-form\" (ngSubmit)=\"onLogin()\"\n                    class=\"my-login-validation m-t-20\" action=\"../achievements\">\n                    <div class=\"m-t-20\">\n                        <div class=\"form-group\">\n                            <input type=\"text\" placeholder=\"Username\" formControlName=\"username\"\n                                class=\"form-control input-radius\" (keyup)=\"trimValue($event)\"\n                                [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\">\n                            <div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\n                                <div *ngIf=\"f.username.errors.required\">Username is required</div>\n                            </div>\n                        </div>\n                        <div class=\"form-group\">\n                            <div style=\"position:relative\">\n                                <input type=\"{{type}}\" placeholder=\"Password\" formControlName=\"password\"\n                                    class=\"form-control input-radius\" class=\"form-control input-radius\"\n                                    [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\"\n                                    style=\"padding-right: 60px;\">\n                                <div class=\"show-btn btn-sm\" (click)=\"toggleShow()\"\n                                    style=\"position:absolute; right:5px; top:19px; transform:translate(0,-50%); padding: 0px 2px; font-size:0px; cursor:pointer;\">\n                                    <mat-icon>visibility</mat-icon>\n                                </div>\n                                <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                                    <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                                    <div *ngIf=\"f.password.errors.minlength\">Password must be at least 6 characters long\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"row mb-2\">\n                            <div class=\"col-6\">\n                                <input type=\"checkbox\" color=\"primary\"> Remember Me\n                            </div>\n                            <div class=\"col-6\">\n                                <button type=\"submit\" class=\"btn btn-primary btn-block input-radius\">\n                                    Login\n                                </button>\n                                <p><a [routerLink]=\"['/privacy_policy']\"><small> Private Policy </small></a></p>\n                            </div>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".b-padding {\n    margin: 3rem;\n}\n.s{\n    .b-padding {      \n            margin: 25px 4rem;\n    }\n}\nh2 {\n    font-size: 25px;\n}\n.card .header h2::before {\n    height: 30px;\n}\n.brand img {\n    max-width: 13rem;\n}\n.brand {\n    position: relative;\n    display: block;\n    margin: 20px 0; \n}\n.h {\n    height: calc(100vh + 70px);\n}\n@media only screen and (max-width: 700px) { \n    .image {\n        display: none;\n    }\n    .b-padding {\n        margin: 4rem 1rem;\n           }\n    .s{\n        .b-padding {      \n            margin: 4rem 1rem;\n        }\n    }\n    .h {\n       display: none;\n    }\n }\n.image {\n     background-image: url(/assets/images/events.jpg);\n    background-repeat: no-repeat;\n    background-size:cover;\n    height: 100%;\n    margin-right: -15px;\n    margin-left: -15px;\n  }\n.overlay:before{\n    position: absolute;\n    content:\" \";\n    top:0;\n    left:0;\n    width:100%;\n    height:100%;\n    display: block;\n    z-index:0;\n    background-color: #00b3a480;\n    /* background-color: #01d8da */\n  }\n.form-radio .mat-radio-label {\n  cursor: pointer;\n}\n.hover-forgot {\n  color: #a0a0a0 !important;\n  margin-top: 3px;\n  font-size: 15px;\n}\n.hover-forgot:hover {\n  border-bottom: 1px solid;\n}\n.login-page {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  position: relative;\n}\n.login-page .content {\n  z-index: 1;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.login-page .content .app-name {\n  margin-top: 0px;\n  padding-bottom: 10px;\n  font-size: 32px;\n}\n.login-page .content .login-form {\n  padding: 40px;\n  background: #fff;\n  width: 500px;\n  box-shadow: 0 0 10px #ddd;\n}\n.login-page .content .login-form input:-webkit-autofill {\n  -webkit-box-shadow: 0 0 0 30px white inset;\n}\n.login-page:after {\n  content: \"\";\n  background: #fff;\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 50%;\n  right: 0;\n}\n.login-page:before {\n  content: \"\";\n  background: #ffffff;\n  position: absolute;\n  top: 50%;\n  left: 0;\n  bottom: 0;\n  right: 0;\n}\n.text-center {\n  text-align: center;\n}\n.w-100 {\n  width: 100%;\n}\n.profile_image {\n  margin: 10rem auto;\n}\n.mla_profile {\n  width: 300px;\n  padding: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIiwiL2hvbWUvYXZvaGkvRG9jdW1lbnRzL21ydl9hZ3JpL2p1bmUxNy9wb2xpdGljaWFuX2FncmljdWx0dXJlX2Zyb250ZW5kL3NyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtBQUNoQjtBQUNBO0lBQ0k7WUFDUSxpQkFBaUI7SUFDekI7QUFDSjtBQUVBO0lBQ0ksZUFBZTtBQUNuQjtBQUNBO0lBQ0ksWUFBWTtBQUNoQjtBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGNBQWM7QUFDbEI7QUFFQTtJQUNJLDBCQUEwQjtBQUM5QjtBQUVDO0lBQ0c7UUFDSSxhQUFhO0lBQ2pCO0lBQ0E7UUFDSSxpQkFBaUI7V0FDZDtJQUNQO1FBQ0k7WUFDSSxpQkFBaUI7UUFDckI7SUFDSjtJQUNBO09BQ0csYUFBYTtJQUNoQjtDQUNIO0FBRUE7S0FDSSxnREFBZ0Q7SUFDakQsNEJBQTRCO0lBQzVCLHFCQUFxQjtJQUNyQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGtCQUFrQjtFQUNwQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxLQUFLO0lBQ0wsTUFBTTtJQUNOLFVBQVU7SUFDVixXQUFXO0lBQ1gsY0FBYztJQUNkLFNBQVM7SUFDVCwyQkFBMkI7SUFDM0IsOEJBQThCO0VBQ2hDO0FDaEVFO0VBQ0ksZUFBQTtBQ0RSO0FETUE7RUFDSSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDSEo7QURNQTtFQUNJLHdCQUFBO0FDSEo7QURNQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDSEo7QURJSTtFQUNJLFVBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQ0ZSO0FER1E7RUFDSSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0FDRFo7QURHUTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtBQ0RaO0FERVk7RUFDSSwwQ0FBQTtBQ0FoQjtBREtJO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0FDSFI7QURLSTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtBQ0hSO0FETUE7RUFDSSxrQkFBQTtBQ0hKO0FES0E7RUFDSSxXQUFBO0FDRko7QURJQTtFQUNJLGtCQUFBO0FDREo7QURLQztFQUNHLFlBQUE7RUFDQSxhQUFBO0FDRkoiLCJmaWxlIjoic3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iLXBhZGRpbmcge1xuICAgIG1hcmdpbjogM3JlbTtcbn1cbi5ze1xuICAgIC5iLXBhZGRpbmcgeyAgICAgIFxuICAgICAgICAgICAgbWFyZ2luOiAyNXB4IDRyZW07XG4gICAgfVxufVxuXG5oMiB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xufVxuLmNhcmQgLmhlYWRlciBoMjo6YmVmb3JlIHtcbiAgICBoZWlnaHQ6IDMwcHg7XG59XG5cbi5icmFuZCBpbWcge1xuICAgIG1heC13aWR0aDogMTNyZW07XG59XG5cbi5icmFuZCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogMjBweCAwOyBcbn1cblxuLmgge1xuICAgIGhlaWdodDogY2FsYygxMDB2aCArIDcwcHgpO1xufVxuXG4gQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3MDBweCkgeyBcbiAgICAuaW1hZ2Uge1xuICAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiAgICAuYi1wYWRkaW5nIHtcbiAgICAgICAgbWFyZ2luOiA0cmVtIDFyZW07XG4gICAgICAgICAgIH1cbiAgICAuc3tcbiAgICAgICAgLmItcGFkZGluZyB7ICAgICAgXG4gICAgICAgICAgICBtYXJnaW46IDRyZW0gMXJlbTtcbiAgICAgICAgfVxuICAgIH1cbiAgICAuaCB7XG4gICAgICAgZGlzcGxheTogbm9uZTtcbiAgICB9XG4gfVxuXG4gLmltYWdlIHtcbiAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvaW1hZ2VzL2V2ZW50cy5qcGcpO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOmNvdmVyO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBtYXJnaW4tcmlnaHQ6IC0xNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiAtMTVweDtcbiAgfVxuXG4gIC5vdmVybGF5OmJlZm9yZXtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgY29udGVudDpcIiBcIjtcbiAgICB0b3A6MDtcbiAgICBsZWZ0OjA7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBoZWlnaHQ6MTAwJTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB6LWluZGV4OjA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwYjNhNDgwO1xuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICMwMWQ4ZGEgKi9cbiAgfVxuIiwiQGltcG9ydCAnLi4vc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzJztcblxuLmZvcm0tcmFkaW8ge1xuICAgIC5tYXQtcmFkaW8tbGFiZWwge1xuICAgICAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgfVxuICAgXG59XG5cbi5ob3Zlci1mb3Jnb3Qge1xuICAgIGNvbG9yOiAjYTBhMGEwICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luLXRvcDogM3B4O1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmhvdmVyLWZvcmdvdDpob3ZlciB7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkO1xufVxuXG4ubG9naW4tcGFnZSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgLmNvbnRlbnQge1xuICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgLmFwcC1uYW1lIHtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgZm9udC1zaXplOiAzMnB4O1xuICAgICAgICB9XG4gICAgICAgIC5sb2dpbi1mb3JtIHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDQwcHg7XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICAgICAgd2lkdGg6IDUwMHB4O1xuICAgICAgICAgICAgYm94LXNoYWRvdzogMCAwIDEwcHggI2RkZDtcbiAgICAgICAgICAgIGlucHV0Oi13ZWJraXQtYXV0b2ZpbGwge1xuICAgICAgICAgICAgICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCAwIDAgMzBweCB3aGl0ZSBpbnNldDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgICY6YWZ0ZXIge1xuICAgICAgICBjb250ZW50OiAnJztcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDA7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIGJvdHRvbTogNTAlO1xuICAgICAgICByaWdodDogMDtcbiAgICB9XG4gICAgJjpiZWZvcmUge1xuICAgICAgICBjb250ZW50OiAnJztcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDUwJTtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICByaWdodDogMDtcbiAgICB9XG59XG4udGV4dC1jZW50ZXIge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi53LTEwMCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4ucHJvZmlsZV9pbWFnZSB7XG4gICAgbWFyZ2luOiAxMHJlbSBhdXRvO1xuICAgXG4gfVxuXG4gLm1sYV9wcm9maWxlIHtcbiAgICB3aWR0aDogMzAwcHg7XG4gICAgcGFkZGluZzogMjBweDtcbiB9XG4iLCJAaW1wb3J0ICcuLi9zaWdudXAvc2lnbnVwLmNvbXBvbmVudC5jc3MnO1xuLmZvcm0tcmFkaW8gLm1hdC1yYWRpby1sYWJlbCB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmhvdmVyLWZvcmdvdCB7XG4gIGNvbG9yOiAjYTBhMGEwICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDNweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uaG92ZXItZm9yZ290OmhvdmVyIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkO1xufVxuXG4ubG9naW4tcGFnZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5sb2dpbi1wYWdlIC5jb250ZW50IHtcbiAgei1pbmRleDogMTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG4ubG9naW4tcGFnZSAuY29udGVudCAuYXBwLW5hbWUge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBmb250LXNpemU6IDMycHg7XG59XG4ubG9naW4tcGFnZSAuY29udGVudCAubG9naW4tZm9ybSB7XG4gIHBhZGRpbmc6IDQwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHdpZHRoOiA1MDBweDtcbiAgYm94LXNoYWRvdzogMCAwIDEwcHggI2RkZDtcbn1cbi5sb2dpbi1wYWdlIC5jb250ZW50IC5sb2dpbi1mb3JtIGlucHV0Oi13ZWJraXQtYXV0b2ZpbGwge1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgMCAwIDMwcHggd2hpdGUgaW5zZXQ7XG59XG4ubG9naW4tcGFnZTphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDUwJTtcbiAgcmlnaHQ6IDA7XG59XG4ubG9naW4tcGFnZTpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAwO1xufVxuXG4udGV4dC1jZW50ZXIge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi53LTEwMCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ucHJvZmlsZV9pbWFnZSB7XG4gIG1hcmdpbjogMTByZW0gYXV0bztcbn1cblxuLm1sYV9wcm9maWxlIHtcbiAgd2lkdGg6IDMwMHB4O1xuICBwYWRkaW5nOiAyMHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/services/api-service */ "./src/app/shared/services/api-service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_5__);






var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, _apiService, formBuilder) {
        this.router = router;
        this._apiService = _apiService;
        this.formBuilder = formBuilder;
        this.type = "password";
        this.show = false;
        this.submitted = false;
    }
    LoginComponent.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.type = "text";
        }
        else {
            this.type = "password";
        }
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.stype = "user";
        this.loginform = this.formBuilder.group({
            username: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            password: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].minLength(6)]]
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () {
            return this.loginform.controls;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.trimValue = function (event) {
        event.target.value = event.target.value.trim();
    };
    LoginComponent.prototype.onChange = function (mrChange) {
        if (mrChange.value) {
            this.checked_type = mrChange.value;
        }
        var mrButton = mrChange.source;
    };
    LoginComponent.prototype.onLogin = function () {
        this.submitted = true;
        if (this.loginform.valid) {
            this._apiService.login(this.loginform.value).subscribe(function (data) {
                if (data.statuscode == 200) {
                    localStorage.setItem("token", data.data["jwt_token"]);
                    localStorage.setItem("currentuser", JSON.stringify(data.data));
                    window.location.replace("/projects");
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Logged in Successfully.",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
                else if (data.statuscode == 203) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Invalid Username and Password. Please try again!!",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
                else if (data.statuscode == 204) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Invalid Username and Password. Please try again!!",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
                else if (data.statuscode == 500) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Something went wrong! Please try again.",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
                else if (data.statuscode == 205) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Your account has been Deactivated, Please contact admin to activate",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
            });
        }
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/shared/guard/auth.guard.ts":
/*!********************************************!*\
  !*** ./src/app/shared/guard/auth.guard.ts ***!
  \********************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function () {
        var currentUser = JSON.parse(localStorage.getItem('currentuser'));
        var token = localStorage.getItem('token');
        if (currentUser && token) {
            return true;
        }
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/services/api-service.ts":
/*!************************************************!*\
  !*** ./src/app/shared/services/api-service.ts ***!
  \************************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/firestore/index.js");





//BackEnd access URL
//const API_URL = 'http://localhost:9000/mla_server/api/v1';
// const API_URL = 'http://mlamrv.five16.com:9000/mla_server/api/v1';
var API_URL = 'http://34.93.47.253:9004/mla_server/api/v1';
var httpOptionsImage = {
    headers: new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-Type': 'multipart/form-data' || false })
};
var httpOptionsClient = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
};
var httpOpt = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
};
var httpOption = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Authorization': localStorage.getItem('token') || '' })
};
var httpOptions = {
    headers: new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Authorization': localStorage.getItem('token') || '' })
};
var ApiService = /** @class */ (function () {
    function ApiService(db, http, httpclient) {
        this.db = db;
        this.http = http;
        this.httpclient = httpclient;
    }
    ApiService_1 = ApiService;
    ApiService.prototype.login = function (data) {
        return this.httpclient.post(API_URL + '/login', data);
    };
    ApiService.prototype.signup = function (data) {
        return this.httpclient.post(API_URL + '/signup', data);
    };
    ApiService.prototype.list_complaints = function () {
        return this.db.collection('complaints', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.getcomplaint = function (complaintid) {
        return this.httpclient.get(API_URL + '/get_complaint?complaint_id=' + complaintid, httpOpt);
    };
    ApiService.prototype.update_complaint_status = function (complaint_id, complaint_status) {
        var body = { complaint_id: complaint_id, complaint_status: complaint_status };
        return this.httpclient.post(API_URL + '/update_complaint_status', body, httpOpt);
    };
    ApiService.prototype.update_complaint_status_description = function (updatedata) {
        var body = { complaint_id: updatedata.complaint_id, complaint_status: updatedata.complaint_status, status_description: updatedata.status_description };
        return this.httpclient.post(API_URL + '/update_complaint_status', body, httpOpt);
    };
    ApiService.prototype.list_complaint_status = function (complaint_status) {
        // const body = { complaint_id:, complaint_status: complaint_status };
        return this.httpclient.get(API_URL + '/list_complaints_complaint_status?complaint_status=' + complaint_status, httpOpt);
    };
    ApiService.prototype.complaint_filter = function (complaint_data) {
        return this.httpclient.post(API_URL + '/complaints_filter', complaint_data, httpOpt);
    };
    ApiService.prototype.getcomplaint_category = function () {
        return this.httpclient.get(API_URL + '/filter_category', httpOpt);
    };
    ApiService.prototype.list_meetings = function () {
        return this.db.collection('meeting_requests', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.deactivate_meeting_request = function (meeting_request_id, is_active) {
        var body = { meeting_request_id: meeting_request_id, is_active: is_active };
        return this.httpclient.post(API_URL + '/deactivate_meeting_request', body, httpOpt);
    };
    ApiService.prototype.meeting_filter = function (complaint_data) {
        return this.httpclient.post(API_URL + '/meeting_filter', complaint_data, httpOpt);
    };
    ApiService.prototype.getmeetingrequest = function (meeting_request_id) {
        return this.httpclient.get(API_URL + '/get_meeting_request?meeting_request_id=' + meeting_request_id, httpOpt);
    };
    ApiService.prototype.get_mla_profile = function (mla_profile_id) {
        return this.httpclient.get(API_URL + '/get_mla_profile?mla_profile_id=' + mla_profile_id, httpOpt);
    };
    ApiService.prototype.edit_mla_profile = function (formData) {
        return this.httpclient.post(API_URL + '/edit_mla_profile', formData, httpOption);
    };
    ApiService.prototype.list_users = function () {
        return this.db.collection('users', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.delete_user = function (data) {
        return this.httpclient.post(API_URL + '/delete_user', data, httpOption);
    };
    ApiService.prototype.get_user = function (user_id) {
        return this.httpclient.get(API_URL + '/get_user?user_id=' + user_id, httpOptionsClient);
    };
    ApiService.prototype.user_filter = function (user_data) {
        return this.httpclient.post(API_URL + '/user_filter', user_data, httpOpt);
    };
    ApiService.prototype.send_notification = function (notify) {
        return this.httpclient.post(API_URL + '/send_message', notify, httpOpt);
    };
    ApiService.prototype.list_achievements = function () {
        return this.db.collection('achievements', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.add_achievement = function (data) {
        return this.httpclient.post(API_URL + '/add_achievement', data, httpOption);
    };
    ApiService.prototype.get_achievement = function (achievement_id) {
        return this.httpclient.get(API_URL + '/get_achievement?achievement_id=' + achievement_id, httpOption);
    };
    ApiService.prototype.edit_achievement = function (updateachievement) {
        return this.httpclient.post(API_URL + '/edit_achievement', updateachievement, httpOption);
    };
    ApiService.prototype.delete_achievement = function (data) {
        return this.httpclient.post(API_URL + '/deactivate_achievement', data, httpOption);
    };
    // News
    ApiService.prototype.list_news = function () {
        return this.db.collection('news', function (ref) {
            return ref.orderBy('news_reported_on', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.add_news = function (data) {
        return this.httpclient.post(API_URL + '/add_news', data, httpOption);
    };
    ApiService.prototype.get_news = function (news_id) {
        return this.httpclient.get(API_URL + '/get_news?news_id=' + news_id, httpOptionsClient);
    };
    ApiService.prototype.edit_news = function (updatenews) {
        return this.httpclient.post(API_URL + '/edit_news', updatenews, httpOption);
    };
    ApiService.prototype.delete_news = function (data) {
        return this.httpclient.post(API_URL + '/deactivate_news', data, httpOption);
    };
    ApiService.prototype.get_all_news_category = function () {
        return this.httpclient.get(API_URL + '/complaint_category', httpOptionsClient);
    };
    ApiService.prototype.list_news_by_category = function (category) {
        return this.httpclient.get(API_URL + '/list_news_by_category?news_category=' + category, httpOptionsClient);
    };
    //Projects
    ApiService.prototype.list_projects = function () {
        return this.db.collection('projects', function (ref) {
            return ref.orderBy('project_start_date', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.getproject = function (project_id) {
        return this.httpclient.get(API_URL + '/get_project?project_id=' + project_id, httpOptionsClient);
    };
    ApiService.prototype.edit_project = function (projectdata) {
        return this.httpclient.post(API_URL + '/edit_project', projectdata, httpOption);
    };
    ApiService.prototype.add_project = function (data) {
        return this.httpclient.post(API_URL + '/add_project', data, httpOption);
    };
    ApiService.prototype.deactivate_project = function (data) {
        return this.httpclient.post(API_URL + '/deactivate_project', data, httpOption);
    };
    ApiService.prototype.projects_filter = function (project_filter_data) {
        return this.httpclient.post(API_URL + '/projects_filter', project_filter_data, httpOpt);
    };
    //events
    ApiService.prototype.list_events = function () {
        return this.db.collection('event', function (ref) {
            return ref.orderBy('event_start_date', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.getevent = function (event_id) {
        return this.httpclient.get(API_URL + '/get_event?event_id=' + event_id, httpOptionsClient);
    };
    ApiService.prototype.deactivate_event = function (data) {
        return this.httpclient.post(API_URL + '/deactivate_event', data, httpOption);
    };
    ApiService.prototype.edit_event = function (eventdata) {
        console.log("evebttasdsad");
        return this.httpclient.post(API_URL + '/edit_event', eventdata, httpOption);
    };
    ApiService.prototype.add_event = function (data) {
        return this.httpclient.post(API_URL + '/add_event', data, httpOption);
    };
    ApiService.prototype.events_filter = function (event_filter_data) {
        return this.httpclient.post(API_URL + '/events_filter', event_filter_data, httpOpt);
    };
    //banner edit and get
    ApiService.prototype.add_banner_images = function (addbanner) {
        return this.httpclient.post(API_URL + '/add_banner_images', addbanner, httpOption);
    };
    //deprtment
    ApiService.prototype.list_deprtment = function () {
        return this.db.collection('departmentcontact', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.list_deprtmenttype = function () {
        return this.db.collection('departmenttype', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.add_department = function (data) {
        return this.httpclient.post(API_URL + '/add_department_contact', data, httpOption);
    };
    ApiService.prototype.edit_department = function (data) {
        return this.httpclient.post(API_URL + '/edit_department_contact', data, httpOption);
    };
    ApiService.prototype.get_department = function (dept_id) {
        return this.httpclient.get(API_URL + '/get_department_contact?department_contact_id=' + dept_id, httpOptionsClient);
    };
    ApiService.prototype.delete_dept = function (data) {
        return this.httpclient.post(API_URL + '/deactivate_department_contact', data, httpOption);
    };
    ApiService.prototype.list_dept_by_type = function (data) {
        return this.httpclient.post(API_URL + '/list_departmentcontact_by_department', data, httpOption);
    };
    //party workers
    ApiService.prototype.list_partyworker = function () {
        return this.db.collection('partyworker', function (ref) {
            return ref.orderBy('created_at', 'desc');
        }).valueChanges();
    };
    ApiService.prototype.add_partyworker = function (data) {
        console.log("ssfsfsf");
        return this.httpclient.post(API_URL + '/add_partyworker', data, httpOption);
    };
    ApiService.prototype.edit_partyworker = function (data) {
        return this.httpclient.post(API_URL + '/edit_partyworker', data, httpOption);
    };
    ApiService.prototype.get_partyworker = function (worker_id) {
        return this.httpclient.get(API_URL + '/get_partyworker?party_worker_id=' + worker_id, httpOptionsClient);
    };
    ApiService.prototype.delete_worker = function (data) {
        return this.httpclient.post(API_URL + '/deactivate_partyworker', data, httpOption);
    };
    ApiService.prototype.filter_by_type = function (data) {
        return this.httpclient.post(API_URL + '/partyworker_filter', data, httpOption);
    };
    var ApiService_1;
    ApiService = ApiService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({ providers: [ApiService_1] }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/signup/signup.component.css":
/*!*********************************************!*\
  !*** ./src/app/signup/signup.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".b-padding {\n    margin: 3rem;\n}\n.s{\n    .b-padding {      \n            margin: 25px 4rem;\n    }\n}\nh2 {\n    font-size: 25px;\n}\n.card .header h2::before {\n    height: 30px;\n}\n.brand img {\n    max-width: 13rem;\n}\n.brand {\n    position: relative;\n    display: block;\n    margin: 20px 0; \n}\n.h {\n    height: calc(100vh + 70px);\n}\n@media only screen and (max-width: 700px) { \n    .image {\n        display: none;\n    }\n    .b-padding {\n        margin: 4rem 1rem;\n           }\n    .s{\n        .b-padding {      \n            margin: 4rem 1rem;\n        }\n    }\n    .h {\n       display: none;\n    }\n }\n.image {\n     background-image: url(/assets/images/events.jpg);\n    background-repeat: no-repeat;\n    background-size:cover;\n    height: 100%;\n    margin-right: -15px;\n    margin-left: -15px;\n  }\n.overlay:before{\n    position: absolute;\n    content:\" \";\n    top:0;\n    left:0;\n    width:100%;\n    height:100%;\n    display: block;\n    z-index:0;\n    background-color: #00b3a480;\n    /* background-color: #01d8da */\n  }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksWUFBWTtBQUNoQjtBQUNBO0lBQ0k7WUFDUSxpQkFBaUI7SUFDekI7QUFDSjtBQUVBO0lBQ0ksZUFBZTtBQUNuQjtBQUNBO0lBQ0ksWUFBWTtBQUNoQjtBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztJQUNkLGNBQWM7QUFDbEI7QUFFQTtJQUNJLDBCQUEwQjtBQUM5QjtBQUVDO0lBQ0c7UUFDSSxhQUFhO0lBQ2pCO0lBQ0E7UUFDSSxpQkFBaUI7V0FDZDtJQUNQO1FBQ0k7WUFDSSxpQkFBaUI7UUFDckI7SUFDSjtJQUNBO09BQ0csYUFBYTtJQUNoQjtDQUNIO0FBRUE7S0FDSSxnREFBZ0Q7SUFDakQsNEJBQTRCO0lBQzVCLHFCQUFxQjtJQUNyQixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGtCQUFrQjtFQUNwQjtBQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxLQUFLO0lBQ0wsTUFBTTtJQUNOLFVBQVU7SUFDVixXQUFXO0lBQ1gsY0FBYztJQUNkLFNBQVM7SUFDVCwyQkFBMkI7SUFDM0IsOEJBQThCO0VBQ2hDIiwiZmlsZSI6InNyYy9hcHAvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmItcGFkZGluZyB7XG4gICAgbWFyZ2luOiAzcmVtO1xufVxuLnN7XG4gICAgLmItcGFkZGluZyB7ICAgICAgXG4gICAgICAgICAgICBtYXJnaW46IDI1cHggNHJlbTtcbiAgICB9XG59XG5cbmgyIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG59XG4uY2FyZCAuaGVhZGVyIGgyOjpiZWZvcmUge1xuICAgIGhlaWdodDogMzBweDtcbn1cblxuLmJyYW5kIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxM3JlbTtcbn1cblxuLmJyYW5kIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiAyMHB4IDA7IFxufVxuXG4uaCB7XG4gICAgaGVpZ2h0OiBjYWxjKDEwMHZoICsgNzBweCk7XG59XG5cbiBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDcwMHB4KSB7IFxuICAgIC5pbWFnZSB7XG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgfVxuICAgIC5iLXBhZGRpbmcge1xuICAgICAgICBtYXJnaW46IDRyZW0gMXJlbTtcbiAgICAgICAgICAgfVxuICAgIC5ze1xuICAgICAgICAuYi1wYWRkaW5nIHsgICAgICBcbiAgICAgICAgICAgIG1hcmdpbjogNHJlbSAxcmVtO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5oIHtcbiAgICAgICBkaXNwbGF5OiBub25lO1xuICAgIH1cbiB9XG5cbiAuaW1hZ2Uge1xuICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoL2Fzc2V0cy9pbWFnZXMvZXZlbnRzLmpwZyk7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6Y292ZXI7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG1hcmdpbi1yaWdodDogLTE1cHg7XG4gICAgbWFyZ2luLWxlZnQ6IC0xNXB4O1xuICB9XG5cbiAgLm92ZXJsYXk6YmVmb3Jle1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBjb250ZW50OlwiIFwiO1xuICAgIHRvcDowO1xuICAgIGxlZnQ6MDtcbiAgICB3aWR0aDoxMDAlO1xuICAgIGhlaWdodDoxMDAlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHotaW5kZXg6MDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDBiM2E0ODA7XG4gICAgLyogYmFja2dyb3VuZC1jb2xvcjogIzAxZDhkYSAqL1xuICB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/signup/signup.component.html":
/*!**********************************************!*\
  !*** ./src/app/signup/signup.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-md-8 h\">\n      <div class=\"image overlay\"></div>\n    </div>\n    <div class=\"col-md-4\">\n      <div class=\"b-padding\">\n        <div class=\"brand \">\n          <font color=#01d8da>\n            <h2 align=\"center\">Events</h2>\n          </font>\n          <div class=\"header m-t-40 text-center\">\n            <small>Singup to continue</small>\n          </div>\n        </div>\n        <form [formGroup]=\"signupForm\" (ngSubmit)=\"onSubmit()\" class=\"my-login-validation m-t-20\">\n          <div class=\"form-group\">\n            <input type=\"text\" placeholder=\"Username\" formControlName=\"username\" class=\"form-control input-radius\" (keyup)=\"trimValue($event)\"\n              [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\">\n            <div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\n              <div *ngIf=\"f.username.errors.required\">Username is required</div>\n            </div>\n          </div>\n          <div class=\"form-group\">\n            <div style=\"position:relative\">\n              <input type=\"{{type}}\" formControlName=\"password\" placeholder=\"Password\" class=\"form-control input-radius\"\n                [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" style=\"padding-right: 60px;\">\n\n              <div class=\"btn-sm\" (click)=\"toggleShow()\"\n                style=\"position: absolute; right: 10px; top: 7px; padding: 0px 2px; font-size: 0px; cursor: pointer;\">\n                <mat-icon>visibility</mat-icon>\n              </div>\n\n              <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                <div *ngIf=\"f.password.errors.minlength\">Password must be at least 6 characters</div>\n              </div>\n            </div>\n          </div>\n         \n        \n          <div class=\"form-group m-0\">\n            <button type=\"submit\" class=\"btn btn-primary btn-block input-radius\">\n              Register\n            </button>\n          </div>\n          <div class=\"header text-center m-t-20\">\n            <small> Already have an account? </small><a [routerLink]=\"['/login']\"> Login</a>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/signup/signup.component.ts":
/*!********************************************!*\
  !*** ./src/app/signup/signup.component.ts ***!
  \********************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/services/api-service */ "./src/app/shared/services/api-service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert */ "./node_modules/sweetalert/dist/sweetalert.min.js");
/* harmony import */ var sweetalert__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert__WEBPACK_IMPORTED_MODULE_5__);






var SignupComponent = /** @class */ (function () {
    function SignupComponent(formBuilder, _apiService, router) {
        this.formBuilder = formBuilder;
        this._apiService = _apiService;
        this.router = router;
        this.type = "password";
        this.show = false;
        this.submitted = false;
    }
    SignupComponent.prototype.toggleShow = function () {
        this.show = !this.show;
        if (this.show) {
            this.type = 'text';
        }
        else {
            this.type = 'password';
        }
    };
    SignupComponent.prototype.trimValue = function (event) {
        event.target.value = event.target.value.trim();
    };
    SignupComponent.prototype.ngOnInit = function () {
        this.signupForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]],
            user_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            institute_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
        });
    };
    Object.defineProperty(SignupComponent.prototype, "f", {
        get: function () { return this.signupForm.controls; },
        enumerable: true,
        configurable: true
    });
    SignupComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.signupForm.valid) {
            this._apiService.signup(this.signupForm.value).subscribe(function (data) {
                _this.signupForm.reset();
                console.log("data.statuscode = ", data.statuscode);
                if (data.statuscode == 200) {
                    _this.router.navigate(['/login']);
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Your account has been created Succesfully",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
                else if (data.statuscode == 203) {
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "User already exist.",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
                else {
                    sweetalert__WEBPACK_IMPORTED_MODULE_5___default()({
                        text: "Something went wrong! Please try again.",
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }
            }, function (err) {
            });
        }
    };
    SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.css */ "./src/app/signup/signup.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], src_app_shared_services_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyCFALwI_loJK-htNOKZd_tDVPPqGRBj8FY",
        authDomain: "politician-agriculture.firebaseapp.com",
        databaseURL: "https://politician-agriculture.firebaseio.com",
        projectId: "politician-agriculture",
        storageBucket: "politician-agriculture.appspot.com",
        messagingSenderId: "5992857661",
        appId: "1:5992857661:web:8a6b00a87485d7b06c6a54"
    }
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/avohi/Documents/mrv_agri/june17/politician_agriculture_frontend/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map