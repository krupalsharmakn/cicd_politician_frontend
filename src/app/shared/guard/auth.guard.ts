import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate() {
        const currentUser = JSON.parse(localStorage.getItem('currentuser'));
        const token = localStorage.getItem('token');

        if (currentUser && token) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }
}
