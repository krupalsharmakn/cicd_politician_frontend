import { Injectable, NgModule } from '@angular/core';
import { Http, Headers, RequestOptionsArgs, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { analyzeAndValidateNgModules } from '@angular/compiler';

//BackEnd access URL
//const API_URL = 'http://localhost:9000/mla_server/api/v1';

// const API_URL = 'http://mlamrv.five16.com:9000/mla_server/api/v1';
const API_URL = 'http://34.93.47.253:9004/mla_server/api/v1';
const httpOptionsImage = {
    headers: new Headers({ 'Content-Type': 'multipart/form-data' || '' })
};

const httpOptionsClient = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
};

const httpOpt = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', 'Authorization': localStorage.getItem('token') || '' })
};

const httpOption = {
    headers: new HttpHeaders({ 'Authorization': localStorage.getItem('token') || '' })
};

const httpOptions = {
    headers: new Headers({ 'Authorization': localStorage.getItem('token') || '' })
};

@NgModule({ providers: [ApiService] })
@Injectable({
    providedIn: 'root'
})

export class ApiService {
    constructor(public db: AngularFirestore, private http: Http, private httpclient: HttpClient) {}
    login(data) {
        return this.httpclient.post(API_URL + '/login', data);
    }

    signup(data) {
        return this.httpclient.post(API_URL + '/signup', data);
    }

    list_complaints() {
        return this.db.collection('complaints', ref =>
        ref.orderBy('created_at','desc')).valueChanges();
    }

    getcomplaint(complaintid) {
        return this.httpclient.get(API_URL + '/get_complaint?complaint_id=' + complaintid, httpOpt)
    }

    update_complaint_status(complaint_id, complaint_status) {
        const body = { complaint_id: complaint_id, complaint_status: complaint_status };
        return this.httpclient.post(API_URL + '/update_complaint_status', body, httpOpt);
    }

 update_complaint_status_description(updatedata) {
        const body = { complaint_id: updatedata.complaint_id, complaint_status: updatedata.complaint_status,status_description:updatedata.status_description};
        return this.httpclient.post(API_URL + '/update_complaint_status', body, httpOpt);
    }
list_complaint_status(complaint_status) {
        // const body = { complaint_id:, complaint_status: complaint_status };
        return this.httpclient.get(API_URL + '/list_complaints_complaint_status?complaint_status=' + complaint_status , httpOpt);
    }

 complaint_filter(complaint_data){
        return this.httpclient.post(API_URL + '/complaints_filter' ,complaint_data, httpOpt);

    }

    getcomplaint_category() {
        return this.httpclient.get(API_URL + '/filter_category', httpOpt)
    }
    list_meetings() {
        return this.db.collection('meeting_requests', ref =>
            ref.orderBy('created_at', 'desc')).valueChanges();
    }

    deactivate_meeting_request(meeting_request_id, is_active) {
        const body = { meeting_request_id: meeting_request_id, is_active: is_active };
        return this.httpclient.post(API_URL + '/deactivate_meeting_request', body, httpOpt);
    }

    meeting_filter(complaint_data){
        return this.httpclient.post(API_URL + '/meeting_filter' ,complaint_data, httpOpt);

    }

    getmeetingrequest(meeting_request_id) {
        return this.httpclient.get(API_URL + '/get_meeting_request?meeting_request_id=' + meeting_request_id, httpOpt)
    }

    get_mla_profile(mla_profile_id) {
        return this.httpclient.get(API_URL + '/get_mla_profile?mla_profile_id=' + mla_profile_id, httpOpt)
    }

    edit_mla_profile(formData) {
        return this.httpclient.post(API_URL + '/edit_mla_profile', formData, httpOption);
    }

    list_users() {
        return this.db.collection('users', ref =>
            ref.orderBy('created_at', 'desc')).valueChanges();
    }

    delete_user(data) {
        return this.httpclient.post(API_URL + '/delete_user', data,httpOption);
    }

    get_user(user_id) {
        return this.httpclient.get(API_URL + '/get_user?user_id=' + user_id, httpOptionsClient);
    }

 user_filter(user_data){
        return this.httpclient.post(API_URL + '/user_filter' ,user_data, httpOpt);

    }


 send_notification(notify){
    return this.httpclient.post(API_URL + '/send_message' ,notify, httpOpt);

}
    list_achievements() {
        return this.db.collection('achievements', ref =>
        ref.orderBy('created_at','desc')).valueChanges();
    }

    add_achievement(data) {
        return this.httpclient.post(API_URL + '/add_achievement', data, httpOption);
    }

    get_achievement(achievement_id) {
        return this.httpclient.get(API_URL + '/get_achievement?achievement_id=' + achievement_id, httpOption);
    }

    edit_achievement(updateachievement) {
        return this.httpclient.post(API_URL + '/edit_achievement', updateachievement, httpOption);
    }

    delete_achievement(data) {
        return this.httpclient.post(API_URL + '/deactivate_achievement', data, httpOption);
    }

    // News
    list_news() {
        return this.db.collection('news', ref =>
        ref.orderBy('news_reported_on','desc')).valueChanges();
    }

    add_news(data) {
        return this.httpclient.post(API_URL + '/add_news', data, httpOption);
    }

    get_news(news_id) {
        return this.httpclient.get(API_URL + '/get_news?news_id=' + news_id, httpOptionsClient);
    }

    edit_news(updatenews) {
        return this.httpclient.post(API_URL + '/edit_news', updatenews, httpOption);
    }

    delete_news(data) {
        return this.httpclient.post(API_URL + '/deactivate_news', data, httpOption);
    }

    get_all_news_category(){
        return this.httpclient.get(API_URL + '/complaint_category' , httpOptionsClient);

    }

    list_news_by_category(category){
        return this.httpclient.get(API_URL + '/list_news_by_category?news_category='+category , httpOptionsClient);

    }

    //Projects
    list_projects() {
        return this.db.collection('projects', ref =>
        ref.orderBy('project_start_date','desc')).valueChanges();
    }

    getproject(project_id) {
        return this.httpclient.get(API_URL + '/get_project?project_id=' + project_id, httpOptionsClient);
    }

    edit_project(projectdata) {
        return this.httpclient.post(API_URL + '/edit_project', projectdata, httpOption);
    }


    add_project(data) {
        return this.httpclient.post(API_URL + '/add_project', data, httpOption);
    }

    deactivate_project(data) {
        return this.httpclient.post(API_URL + '/deactivate_project', data, httpOption);
    }

    projects_filter(project_filter_data){
        return this.httpclient.post(API_URL + '/projects_filter' ,project_filter_data, httpOpt);

    }

    //events
    list_events() {
        return this.db.collection('event', ref =>
        ref.orderBy('event_start_date','desc')).valueChanges();
    }

    getevent(event_id) {
        return this.httpclient.get(API_URL + '/get_event?event_id=' + event_id, httpOptionsClient);
    }

    deactivate_event(data) {
        return this.httpclient.post(API_URL + '/deactivate_event', data, httpOption);
    }

    edit_event(eventdata) {
        console.log("evebttasdsad")
        return this.httpclient.post(API_URL + '/edit_event', eventdata, httpOption);
    }

    add_event(data) {
        return this.httpclient.post(API_URL + '/add_event', data, httpOption);
    }

     events_filter(event_filter_data){
        return this.httpclient.post(API_URL + '/events_filter' ,event_filter_data, httpOpt);

    }
    //banner edit and get

    add_banner_images(addbanner) {
        return this.httpclient.post(API_URL + '/add_banner_images', addbanner, httpOption);
    }


    //deprtment

    list_deprtment() {
        return this.db.collection('departmentcontact', ref =>
        ref.orderBy('created_at','desc')).valueChanges();
    }

    list_deprtmenttype() {
        return this.db.collection('departmenttype', ref =>
        ref.orderBy('created_at','desc')).valueChanges();
    }
    add_department(data) {
        return this.httpclient.post(API_URL + '/add_department_contact', data, httpOption);
    }
    
    edit_department(data) {
        return this.httpclient.post(API_URL + '/edit_department_contact', data, httpOption);
    }
    get_department(dept_id){
        return this.httpclient.get(API_URL + '/get_department_contact?department_contact_id=' + dept_id, httpOptionsClient);

    }
    delete_dept(data){
        return this.httpclient.post(API_URL + '/deactivate_department_contact', data, httpOption);
    }
    list_dept_by_type(data){
        return this.httpclient.post(API_URL + '/list_departmentcontact_by_department', data, httpOption);

    }

    //party workers

    list_partyworker() {
        return this.db.collection('partyworker', ref =>
        ref.orderBy('created_at','desc')).valueChanges();
    }

    add_partyworker(data) {
        console.log("ssfsfsf")
        return this.httpclient.post(API_URL + '/add_partyworker', data, httpOption);
    }

    edit_partyworker(data) {
        return this.httpclient.post(API_URL + '/edit_partyworker', data, httpOption);
    }

    get_partyworker(worker_id){
        return this.httpclient.get(API_URL + '/get_partyworker?party_worker_id=' + worker_id, httpOptionsClient);

    }
    delete_worker(data){
        return this.httpclient.post(API_URL + '/deactivate_partyworker', data, httpOption);
    }

    filter_by_type(data){
        return this.httpclient.post(API_URL + '/partyworker_filter', data, httpOption);

    }


}
