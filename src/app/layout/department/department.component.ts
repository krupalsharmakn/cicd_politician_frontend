import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import {ApiService} from '../../shared/services/api-service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as XLSX from 'xlsx';
import {Router} from '@angular/router';
import swal from 'sweetalert';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('tableloadedvalue') table: ElementRef;
  @ViewChild('tablefilterloadedvalue') tablefilter: ElementRef;
  dataSource: any;
  excelData:any;
  deptlist;
  loader = false;
  dept_editid;
  displayedColumns: string[] = ['sno', 'department', 'email','phone_number','contact_person', 'action'];
  displayedExcelColumns: string[] = ['sno', 'dept_type', 'email_id','phone_number','alernative_number','contact_person','address'];
  filterformenable = false;
  filterform: FormGroup;
  dept_type_data;
  category_list;
  taluk_list;
  ward_list;
  district_list;
  deptdatalist;
  department_type=[
    { name:'water' },
    { name:'electricity' },
   
  ]
  
  constructor(private apiService: ApiService, private formBuilder: FormBuilder,private router: Router,) { }

  ngOnInit() {
    this.list_department();
    this.getcategory_list();
    this. list_deprtmenttype();
    localStorage.removeItem('editdeptid')
    this.filterform=this.formBuilder.group({
      department_type:['', Validators.required],
      panchayath_ward:['', Validators.required],
      taluk:['', Validators.required],
      district:['', Validators.required],
      pincode:['', Validators.required],
    })  }


    list_deprtmenttype(){
      this.apiService.list_deprtmenttype().subscribe(res => {
        this.deptdatalist = res;
       console.log("dfsfsf", this.deptdatalist)

      },
      err => console.error(err)
  );
    }

  list_department() {
    this.apiService.list_deprtment().subscribe(res => {
          this.deptlist = res;
          console.log("this.deptlist",this.deptlist)
          this.loader = false;
          this.dataSource = new MatTableDataSource();
          this.excelData = new MatTableDataSource();
          this.excelData.data=this.deptlist;
          this.dataSource.data = this.deptlist;
          this.dataSource.sort = this.sort;
          this.dataSource.sortingDataAccessor = (data, attribute) => data[attribute];
          this.dataSource.paginator = this.paginator;
          console.log("lissttt", this.deptlist)

        },
        err => console.error(err)
    );
  }

  onView(dept_id) {
    console.log("iddd",dept_id)
    localStorage.setItem('dept_id', dept_id);
  this.router.navigate(['/viewdepartment']);
  }

  get f() { return this.filterform.controls; }

  onopenHandled() {
    this.filterformenable = true;
  }
  onCloseHandled() {
    this.filterformenable = false;
    this.filterform.reset()

    // this.filterform.setValue({
    //   close_status: ''
    // })

    this.filterform.setValue({
      department_type: '',
    })     
    this.list_department()
    // this.table = true;
  }

  dept_filter(){
    console.log("news ''filter''",this.filterform.value.department_type);
    var department_type={
      department:this.filterform.value.department_type,
      taluk:this.filterform.value.taluk,
      district:this.filterform.value.district,
      panchayath_ward:this.filterform.value.panchayath_ward,
      pin:this.filterform.value.pincode
    }
    this.apiService.list_dept_by_type(department_type).subscribe(res => {
     this.dept_type_data=res['data'];
       console.log("res",this.dept_type_data);
       this.dataSource = new MatTableDataSource();
       this.dataSource = new MatTableDataSource();
       this.excelData = new MatTableDataSource();
       this.excelData.data=this.dept_type_data;
       this.dataSource.data = this.dept_type_data;
       this.dataSource.sort = this.sort;
       this.dataSource.paginator = this.paginator;

      },
      err => console.error(err)
  );
  }


  getcategory_list(){
    console.log("334343fffffffg")
    this.apiService.getcomplaint_category().subscribe(
      res => {
        this.category_list = res;
        this.district_list = this.category_list.complaint_categories.district;
      },
      err => console.error(err)
  );
  }


  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
    var datalist = this.district_list.find(o => o.name == eventdata.target.value) || '';
    this.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)

    // var datalist=this.taluk_list.findIndex(item=>item.taluk_name===eventdata.target.value);
    this.ward_list = datalist.town_panchayat
    console.log("datalist", this.ward_list)
  }


  loadPanchayat(eventdata: any){
    var datalist = this.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.ward_list = datalist.panchayath_ward;
  }

  upDatepin(eventdata){
    var datalist = this.ward_list.find(o => o.name == eventdata.target.value) || '';
    console.log("datalist",datalist.pin)
    // this.pinvalue=datalist.pin;
    this.filterform.controls.pincode.setValue(datalist.pin)

  }
  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'departmentlist.xlsx');
  }

  onEdit(deptid){
    this.dept_editid=deptid;
    localStorage.setItem('editdeptid', deptid);
     this.router.navigate(['/adddepartment']);

  }
  
  onDelete(dept_id, is_active) {
    var data = {
      department_contact_id: dept_id,
      is_active: is_active
    };
    if (is_active == true) {
      swal({
        text: 'Are you sure?. Confirm to Activate the Department.',
        buttons: ['Cancel', 'Ok'],
        dangerMode: true
      })
          .then((value) => {
            if (value) {
              this.apiService.delete_dept(data).subscribe((data: any) => {
                if (data.statuscode = 204) {
                  this.list_department();
                  swal({
                    text: 'Department Activated Successfully.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                } else {
                  swal({
                    text: 'Something went wrong! Please try again.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                }
              });
            }
          });
    } else {
      swal({
        text: 'Are you sure?. Confirm to Deactivate the Department.',
        buttons: ['Cancel', 'Ok'],
        dangerMode: true
      })
          .then((value) => {
            if (value) {
              this.apiService.delete_dept(data).subscribe((data: any) => {
                if (data.statuscode = 204) {
                  this.list_department();
                  swal({
                    text: 'Department Deactivated Successfully.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                } else {
                  swal({
                    text: 'Something went wrong! Please try again.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                }
              });
            }
          });
    }
  }

  

  sortData(sort: MatSort) {
    const data = this.deptlist.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'department': return compare(a.department, b.department, isAsc);
        case 'email': return compare(a.email, b.email, isAsc);
        case 'phone_number': return compare(a.phone_number, b.phone_number, isAsc);
        case 'contact_person': return compare(a.contact_person, b.contact_person, isAsc);
        default: return 0;
      }
    });
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) { 
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}