import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
import { Location } from '@angular/common';

@Component({
  selector: 'app-adddepartment',
  templateUrl: './adddepartment.component.html',
  styleUrls: ['./adddepartment.component.css']
})
export class AdddepartmentComponent implements OnInit {
  adddeptform: FormGroup;
  submitted = false;
  deptdata;
  editdept;
  deptlist;
  category_list;
  taluk_list;
  ward_list;
  district_list;
  editdata = false;
  wardname;
  pinvalue;
  talukname;
  constructor(private apiService: ApiService, private router: Router, private formBuilder: FormBuilder, private location: Location) { }

  ngOnInit() {
    this.list_department();
    this.getcategory_list();
    this.adddeptform = this.formBuilder.group({
      dept_id: [''],
      dept_type: ['', Validators.required],
      email_id: ['', [Validators.required, Validators.email]],
      phone_number: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      alternative_number: ['', Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")],
      address: ['', Validators.required],
      taluk: ['', Validators.required],
      district: ['', Validators.required],
      // state: ['', Validators.required],
      ward: ['', Validators.required],
      pincode: ['', Validators.required],
      contact_person: [''],
    });
    console.log("***********************")
    this.editdept = localStorage.getItem('editdeptid');
    console.log("this.editdept", this.editdept)
    if (this.editdept) {
      this.editdata = true;
      this.getdepartment();

    }

  }

  list_department(){
    this.apiService.list_deprtmenttype().subscribe(res => {
      this.deptlist = res;

    },
    err => console.error(err)
);
  }

  // list_department() {
  //   this.apiService.list_deprtment().subscribe(res => {
  //     this.deptlist = res;
  //     console.log("lissttt", this.deptlist)
  //   },
  //     err => console.error(err)
  //   );
  // }

  get f() { return this.adddeptform.controls; }


  getdepartment() {
    console.log("334343fffffffg")
    this.apiService.get_department(this.editdept).subscribe(
      res => {
        this.deptdata = res;
        console.log("dept_id", this.deptdata.data)
        //  this.deptdata = this.deptdata.data;
        const deptdatas = this.deptdata.data;
        this.adddeptform.setValue({
          dept_id: deptdatas.department_contact_id,
          dept_type: deptdatas.department,
          email_id: deptdatas.email,
          phone_number: deptdatas.phone_number,
          alternative_number: deptdatas.alternative_number,
          address: deptdatas.address,
          contact_person: deptdatas.contact_person,
          taluk: deptdatas.taluk,
          district: deptdatas.district,
          ward: deptdatas.panchayath_ward,
          pincode: deptdatas.pin,
          // state:deptdatas.state


        })
        this.wardname = this.adddeptform.value.ward;
        this.talukname= this.adddeptform.value.taluk;
        console.log(" this.talukname", this.talukname)

      },
      err => console.error(err)
    );
  }



  getcategory_list() {
    console.log("334343fffffffg")
    this.apiService.getcomplaint_category().subscribe(
      res => {
        this.category_list = res;
        this.district_list = this.category_list.complaint_categories.district;
        console.log("this.taluk_list", this.taluk_list)
      },
      err => console.error(err)
    );
  }


  loadTaluk(eventdata: any) {
    this.editdata = false;
    this.adddeptform.controls.pincode.setValue('')
    console.log("eventdata", eventdata.target.value);
    var datalist = this.district_list.find(o => o.name == eventdata.target.value) || '';
    this.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)

    // var datalist=this.taluk_list.findIndex(item=>item.taluk_name===eventdata.target.value);
    this.ward_list = datalist.town_panchayat
    console.log("datalist", this.ward_list)
  }


  loadPanchayat(eventdata: any){
    this.editdata = false;
    var datalist = this.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.ward_list = datalist.panchayath_ward;
  }

  upDatepin(eventdata){
    var datalist = this.ward_list.find(o => o.name == eventdata.target.value) || '';
    console.log("datalist",datalist.pin)
    this.adddeptform.controls.pincode.setValue(datalist.pin)

  }
  onSubmit() {
    this.submitted = true;
    const adddept: FormData = new FormData();
    adddept.append('department', this.adddeptform.value.dept_type);
    adddept.append('email', this.adddeptform.value.email_id);
    adddept.append('phone_number', this.adddeptform.value.phone_number);
    adddept.append('alternative_number', this.adddeptform.value.alternative_number);
    adddept.append('address', this.adddeptform.value.address);
    adddept.append('contact_person', this.adddeptform.value.contact_person);
    adddept.append('taluk', this.adddeptform.value.taluk);
    adddept.append('district', this.adddeptform.value.district);
    adddept.append('panchayath_ward', this.adddeptform.value.ward);
    adddept.append('pin', this.adddeptform.value.pincode);


    if (this.adddeptform.valid) {

      if (this.adddeptform.value.dept_id) {
        const editdept: FormData = new FormData();
        editdept.append('department_contact_id', this.editdept);
        editdept.append('department', this.adddeptform.value.dept_type);
        editdept.append('email', this.adddeptform.value.email_id);
        editdept.append('phone_number', this.adddeptform.value.phone_number);
        editdept.append('alternative_number', this.adddeptform.value.alternative_number);
        editdept.append('address', this.adddeptform.value.address);
        editdept.append('contact_person', this.adddeptform.value.contact_person);
        editdept.append('taluk', this.adddeptform.value.taluk);
        editdept.append('district', this.adddeptform.value.district);
        editdept.append('panchayath_ward', this.adddeptform.value.ward);
        editdept.append('pin', this.adddeptform.value.pincode);
        this.apiService.edit_department(editdept).subscribe((data: any) => {
          console.log("data", data)
          if (data.statuscode == 200) {
            swal({
              text: 'Department updated Successfully.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
            this.router.navigate(['/department']);
            this.adddeptform.reset();
          } else {
            swal({
              text: 'Something went wrong! Please try again.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          }
        });
      } else {
        this.apiService.add_department(adddept).subscribe((data: any) => {
          console.log("data", data)
          if (data.statuscode == 200) {
            swal({
              text: 'Department added Successfully.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
            this.router.navigate(['/department']);
            this.adddeptform.reset();
          } else {
            swal({
              text: 'Something went wrong! Please try again.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          }
        });
      }

    } else {
      swal({
        text: 'Please fill all the details',
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }
  backClicked() {
    this.location.back();
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
