import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
import { Location } from '@angular/common';
@Component({
  selector: 'app-viewdepartment',
  templateUrl: './viewdepartment.component.html',
  styleUrls: ['./viewdepartment.component.css']
})
export class ViewdepartmentComponent implements OnInit {
  dept_id;
  deptdata;
  dept_list;
  constructor(private apiService: ApiService, private router: Router, private formBuilder: FormBuilder,private location: Location) { }

  ngOnInit() {
    this.dept_id = localStorage.getItem('dept_id');
    this.getdepartment()
  }

  getdepartment(){
    this.apiService.get_department(this.dept_id).subscribe(
      res => {
      this.dept_list = res
       this.deptdata = this.dept_list.data;
       console.log("dept_id",this.deptdata.is_active)

  
      },
      err => console.error(err)
  );
  }
  backClicked() {
    this.location.back();
  }

  onEdit(){
    localStorage.setItem('editdeptid', this.dept_id);
     this.router.navigate(['/adddepartment']);

  }
  onDelete(dept_id, is_active) {
    console.log("newssssss",dept_id,"asdasdasdasd",is_active)
    var data = {
      department_contact_id: dept_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the department.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_dept(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getdepartment();
                            swal({
                                text: 'Department Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the department.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_dept(data).subscribe((data: any) => {
                      console.log("datatatt",data.statuscode)
                        if (data.statuscode = 204) {
                          this.getdepartment();
                            swal({
                                text: 'Department Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    }
  }
  
}
