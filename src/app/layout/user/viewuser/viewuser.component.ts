import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css']
})
export class ViewuserComponent implements OnInit {
  public user_id;
  public userdata;

  constructor(private location: Location,private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    this.user_id = localStorage.getItem('user_id');
    this.getmeetingrequest()

  }

  getmeetingrequest() {
    this.apiService.get_user(this.user_id).subscribe(
      res => {
          this.userdata = res;
          this.userdata = this.userdata.data;
          console.log("usersasdata",this.userdata)
      },
      err => console.error(err)
  );
  }

  backClicked() {
    this.location.back();
  }


  onDelete(user_id, is_active) {
    var data = {
      user_id: user_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the user.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_user(data).subscribe((data: any) => {
                      console.log("sadsadsad",data)
                        if (data.statuscode = 204) {
                          this.getmeetingrequest();
                            swal({
                                text: 'User Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the User.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_user(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getmeetingrequest();
                            swal({
                                text: 'User Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    }
  }
  
}
