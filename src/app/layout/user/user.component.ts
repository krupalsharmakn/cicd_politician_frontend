import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Location } from '@angular/common';

import {
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/animations';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
    animal: string;
    name: string;
}

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css'],
    animations: [
        trigger('slideInOut', [
            state(
                'in',
                style({
                    transform: 'translate3d(0, 0, 0)'
                })
            ),
            state(
                'out',
                style({
                    transform: 'translate3d(100%, 0, 0)'
                })
            ),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out'))
        ])
    ]

})
@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
    public items: Array<any>;
    public userdata;
    public dialog_user_id;
    toggleForm = false;
    animal: string;
    name: string;
    @ViewChild('tableloadedvalue') table: ElementRef;
    @ViewChild('tablefilterloadedvalue') tablefilter: ElementRef;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    displayedexcelColumns: string[] = ['sno', 'name', 'email', 'mobile_number', 'marital_status', 'age', 'blood_group', 'pincode', 'ward_no'];
    datatable: boolean = true;

    displayedColumns: string[] = ['sno', 'name', 'email', 'mobile_number', 'age', 'action'];
    places: Array<any> = [];
    slideState = 'out';
    public toggleenable: Boolean = false;
    exceldata: any;
    filtertable: boolean = false;
    filterdataSource: any;
    dataSource: any;
    public userslist;
    loader = false;
    filterform: FormGroup;
    filterformenable = false;


    constructor(private apiService: ApiService, private router: Router, private formBuilder: FormBuilder, public dialog: MatDialog) {
    }

    ngOnInit() {
        this.list_users();
        // this.filter_users();
        this.filterform = this.formBuilder.group({
            is_active: ['', Validators.required],
            age: ['', Validators.required],
            marital_status: ['', Validators.required],
            pincode: ['', Validators.required],
            blood_group: ['', Validators.required],
            gender: ['', Validators.required],
            ward_no: ['', Validators.required],

        });

    }
   

    openAlluserdialog(): void {


        const dialogRef = this.dialog.open(DialogAlluserNotification, {
            width: '250px',
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.animal = result;
        });
    }

    openDialog(userid): void {

        this.dialog_user_id = userid;
        localStorage.setItem('dialog_user_id', userid);

        const dialogRef = this.dialog.open(DialogNotification, {
            width: '250px',
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
            this.animal = result;
        });
    }
    list_users() {
        this.apiService.list_users().subscribe(res => {
            this.userslist = res;
            this.loader = false;
            console.log("userssss", this.userslist)
            this.dataSource = new MatTableDataSource();
            this.exceldata = new MatTableDataSource();
            this.exceldata.data = this.userslist;
            this.dataSource.data = this.userslist;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        },
            err => console.error(err)
        );
    }

    toggleMenu(user_id) {
        this.toggleenable = true;
        this.slideState = this.slideState === 'out' ? 'in' : 'out';
        this.apiService.get_user(user_id).subscribe(
            res => {
                this.userdata = res;
                this.userdata = this.userdata.data;
            },
            err => console.error(err)
        );
    }


    onView(user_id) {
        localStorage.setItem('user_id', user_id);
        this.router.navigate(['/viewuser']);
    }
    userfilter() {
        var data = {
            is_active: this.filterform.value.is_active,
            age: this.filterform.value.age,
            marital_status: this.filterform.value.marital_status,
            pincode: this.filterform.value.pincode,
            blood_group: this.filterform.value.blood_group,
            gender: this.filterform.value.gender,
            ward_no: this.filterform.value.ward_no
        }
        this.datatable = false;
        this.filtertable = true;
        this.apiService.user_filter(data).subscribe(res => {
            this.userslist = res['data'];
            this.loader = false;
            this.dataSource = new MatTableDataSource();
            this.filterdataSource = new MatTableDataSource();
            this.filterdataSource = this.userslist
            this.dataSource.data = this.userslist;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        },
            err => console.error(err)
        );

    }

    toggleclose() {
        this.slideState = this.slideState === 'in' ? 'out' : 'in';
        this.userdata = '';
        this.toggleenable = false;
    }

    onDelete(user_id, is_active) {
        var data = {
            user_id: user_id,
            is_active: is_active
        };
        if (is_active == true) {
            swal({
                text: 'Are you sure?. Confirm to activate the User.',
                buttons: ['Cancel', 'Ok'],
                dangerMode: true
            })
                .then((value) => {
                    if (value) {
                        this.apiService.delete_user(data).subscribe((data: any) => {
                            if (data.statuscode = 204) {
                                this.list_users();
                                swal({
                                    text: 'User Activated Successfully.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            } else {
                                swal({
                                    text: 'Something went wrong! Please try again.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            }
                        });
                    }
                });
        } else {
            swal({
                text: 'Are you sure?. Confirm to deactivate the User.',
                buttons: ['Cancel', 'Ok'],
                dangerMode: true
            })
                .then((value) => {
                    if (value) {
                        this.apiService.delete_user(data).subscribe((data: any) => {
                            if (data.statuscode = 204) {
                                this.list_users();
                                swal({
                                    text: 'User Deactivated Successfully.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            } else {
                                swal({
                                    text: 'Something went wrong! Please try again.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            }
                        });
                    }
                });
        }
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    onCloseHandled() {
        this.filterformenable = false;
        this.filterform.reset()
        this.list_users();
        this.filtertable = false;
        this.datatable = true;
        this.filterform.setValue({
            is_active: '',
            age: '',
            marital_status: '',
            pincode: '',
            blood_group: '',
            gender: '',
            ward_no: ''


        })

    }
    onopenHandled(tag) {
        this.filterformenable = true;
        // this.table = false;
    }


    exceldataimport() {
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
        console.log("this.table.nativeElement", this.table.nativeElement)
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, 'Userdata.xlsx');
    }

    exceldatafilterimport() {
        console.log("sdfsdfds")
        const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tablefilter.nativeElement);
        const wb: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

        /* save to file */
        XLSX.writeFile(wb, 'Userdata.xlsx');
    }
}

@Component({
    selector: 'dialog-notification',
    templateUrl: 'dialog-notification.html',
})
export class DialogNotification {
    public dialog_user_id;
    public dialogform: FormGroup;
    public result;
    constructor(
        public dialogRef: MatDialogRef<DialogNotification>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData, private router: Router, private formBuilder: FormBuilder, private apiService: ApiService) { }
    ngOnInit() {
        this.dialog_user_id = localStorage.getItem('dialog_user_id')
        console.log("dialog_user_id  ", this.dialog_user_id);
        this.dialogform = this.formBuilder.group({
            user_id: [''],
            notification_title: ['', Validators.required],
            notification_message: ['', Validators.required],
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
    onSubmit() {
        var notifydata = {
            to: "individual",
            user_id: this.dialog_user_id,
            notification_title: this.dialogform.value.notification_title,
            notification_message: this.dialogform.value.notification_message
        }
        this.apiService.send_notification(notifydata).subscribe(
            res => {
                this.result = res;
                console.log("result", this.result.statuscode)
                if (this.result.statuscode == 200) {
                    // this.router.navigate(['/users']);
                    this.dialogRef.close();

                    swal({
                        text: 'Notification Sent Successfully.',
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                } else {
                    swal({
                        text: 'Something went wrong! Please try again.',
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }



            },
            err => console.error(err)
        );

    }

}


@Component({
    selector: 'dialog-alluser',
    templateUrl: 'dialog-alluser.html',
})
export class DialogAlluserNotification {
    public dialog_user_id;
    public dialogform: FormGroup;
    public result;
    constructor(
        public dialogRef: MatDialogRef<DialogAlluserNotification>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData, private router: Router, private formBuilder: FormBuilder, private apiService: ApiService) { }
    ngOnInit() {
        this.dialogform = this.formBuilder.group({
            user_id: [''],
            notification_title: ['', Validators.required],
            notification_message: ['', Validators.required],
        });
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
    onSubmit() {
        var notifydata = {
            to: "all",
            user_id: '',
            notification_title: this.dialogform.value.notification_title,
            notification_message: this.dialogform.value.notification_message
        }
        this.apiService.send_notification(notifydata).subscribe(
            res => {
                this.result = res;
                if (this.result.statuscode == 200) {
                    // this.router.navigate(['/users']);
                    this.dialogRef.close();

                    swal({
                        text: 'Notification Sent Successfully.',
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                } else {
                    swal({
                        text: 'Something went wrong! Please try again.',
                        buttons: [false],
                        dangerMode: true,
                        timer: 3000
                    });
                }



            },
            err => console.error(err)
        );

    }

}