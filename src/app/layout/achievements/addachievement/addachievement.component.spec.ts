import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddachievementComponent } from './addachievement.component';

describe('AddachievementComponent', () => {
  let component: AddachievementComponent;
  let fixture: ComponentFixture<AddachievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddachievementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddachievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
