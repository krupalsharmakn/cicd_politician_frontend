import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
import { Location } from '@angular/common';
@Component({
  selector: 'app-addachievement',
  templateUrl: './addachievement.component.html',
  styleUrls: ['./addachievement.component.css']
})
export class AddachievementComponent implements OnInit {

  addachievement: FormGroup;
  public profileimage;
  public uploadimage;
  loader = false;
  submitted = false;
  constructor(private location: Location,private apiService: ApiService, private router: Router, private formBuilder: FormBuilder) { }
  ngOnInit() {
    const file: File = this.profileimage;
    this.addachievement = this.formBuilder.group({
      achievement_title: ['', Validators.required],
      achievement_short_description: ['', Validators.required],
      achievement_description: ['', Validators.required],
      achievement_duration: ['', Validators.required],
      image: ['']
    });
  }

  get f() { return this.addachievement.controls; }
  onSubmit() {
    const updateachievement: FormData = new FormData();
    const file: File = this.profileimage;
    updateachievement.append('achievement_id', this.addachievement.value.achievement_id);
    updateachievement.append('achievement_short_description', this.addachievement.value.achievement_short_description);
    updateachievement.append('achievement_description', this.addachievement.value.achievement_description);
    updateachievement.append('achievement_title', this.addachievement.value.achievement_title);
    updateachievement.append('achievement_duration', this.addachievement.value.achievement_duration);
    updateachievement.append('image', file);
    this.submitted = true;
    if (this.addachievement.valid) {
      this.loader = false;
      this.apiService.add_achievement(updateachievement).subscribe((data: any) => {
        if (data.statuscode == 200) {
          swal({
            text: "Achievement added Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
          this.router.navigate(['/achievements']);
          this.addachievement.reset();
        } else {
          swal({
            text: "Something went wrong! Please try again.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    } else {
      swal({
        text: "Please fill all the details",
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }

  fileChange(e) {
    const profiles = e.target.files[0];
    this.profileimage = profiles;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(profiles);
  }

  backClicked() {
    this.location.back();
  }
}
