import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api-service';
import swal from 'sweetalert';
import { log } from 'util';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-editachievement',
  templateUrl: './editachievement.component.html',
  styleUrls: ['./editachievement.component.css']
})
export class EditachievementComponent implements OnInit {

  editachievementform: FormGroup
  public achievementdata;
  public currentuser;
  public image;
  public uploadimage;
  public profileimage;
  loader = false;
  private submitted: boolean;
  images_array = "http://";
  constructor(private router: Router, public apiService: ApiService, private formBuilder: FormBuilder, private location: Location) {
  }

  ngOnInit() {
    this.getachievement();

    this.editachievementform = this.formBuilder.group({
      achievement_id: ['', Validators.required],
      achievement_short_description: ['', Validators.required],
      achievement_description: ['', Validators.required],
      achievement_title: ['', Validators.required],
      achievement_duration: ['', Validators.required],
    });
  }

  getachievement() {
    const achievement_id = localStorage.getItem("achievement_id");
    this.loader = true;
    this.apiService.get_achievement(achievement_id).subscribe(
      res => {
        this.achievementdata = res;
        this.loader = false;
        if (this.achievementdata.statuscode == 200) {
          const achievementdatas = this.achievementdata.data;
          this.uploadimage = achievementdatas.achievement_images || '';
          this.editachievementform.setValue({
            achievement_id: achievementdatas.achievement_id,
            achievement_short_description: achievementdatas.achievement_short_description || '',
            achievement_description: achievementdatas.achievement_description || '',
            achievement_title: achievementdatas.achievement_title || '',
            achievement_duration: achievementdatas.achievement_duration || ''
          });
        }
      },
      err => console.error(err)
    );
  }

  fileChange(e) {
    const profiles = e.target.files[0];
    this.profileimage = profiles;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(profiles);
  }


  onSubmit() {
    const updateachievement: FormData = new FormData();
    const file: File = this.profileimage;
    console.log('file', file);
    updateachievement.append('achievement_id', this.editachievementform.value.achievement_id);
    updateachievement.append('achievement_short_description', this.editachievementform.value.achievement_short_description);
    updateachievement.append('achievement_description', this.editachievementform.value.achievement_description);
    updateachievement.append('achievement_title', this.editachievementform.value.achievement_title);
    updateachievement.append('achievement_duration', this.editachievementform.value.achievement_duration);
    updateachievement.append('image', file);
    this.submitted = true;
    if (this.editachievementform.valid) {
      console.log('coming here');
      this.apiService.edit_achievement(updateachievement).subscribe((res: any) => {
        if (res.statuscode == 200) {
          swal({
            text: 'Achievement Updated Successfully.',
            buttons: [false],
            dangerMode: true,
            //timer: 3000
          });
          this.router.navigate(['/achievements']);
        } else {
          swal({
            text: 'Something went wrong! Please try again.',
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    } else {
      swal({
        text: 'Please fill all the details',
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }

  backClicked() {
    this.location.back();
  }
}
