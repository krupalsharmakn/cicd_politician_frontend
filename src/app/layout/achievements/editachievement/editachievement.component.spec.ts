import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditachievementComponent } from './editachievement.component';

describe('EditachievementComponent', () => {
  let component: EditachievementComponent;
  let fixture: ComponentFixture<EditachievementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EditachievementComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditachievementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
