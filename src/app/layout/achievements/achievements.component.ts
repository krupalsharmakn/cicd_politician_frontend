import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Location } from '@angular/common';

import {
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/animations';
import { Router } from '@angular/router';
import swal from 'sweetalert';

@Component({
    selector: 'app-achievements',
    templateUrl: './achievements.component.html',
    styleUrls: ['./achievements.component.css'],
    animations: [
        trigger('slideInOut', [
            state(
                'in',
                style({
                    transform: 'translate3d(0, 0, 0)'
                })
            ),
            state(
                'out',
                style({
                    transform: 'translate3d(100%, 0, 0)'
                })
            ),
            transition('in => out', animate('400ms ease-in-out')),
            transition('out => in', animate('400ms ease-in-out'))
        ])
    ]
})
export class AchievementsComponent implements OnInit {
    public items: Array<any>;
    public achievementdata;
    toggleForm = false;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    displayedColumns: string[] = ['sno', 'achievement_title', 'achievement_short_description', 'achievement_duration', 'action'];
    places: Array<any> = [];
    slideState = 'out';
    public toggleenable: Boolean = false;
    dataSource: any;
    public achievementslist;
    loader = false;
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }

    constructor(private location: Location, private apiService: ApiService, private router: Router) {
    }

    ngOnInit() {
        this.list_achievements();
    }

    list_achievements() {
        this.apiService.list_achievements().subscribe(res => {
            this.achievementslist = res;
            console.log(this.achievementslist);
            this.loader = false;
            this.dataSource = new MatTableDataSource();
            this.dataSource.data = this.achievementslist;
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
        },
            err => console.error(err)
        );
    }

    toggleMenu(achievement_id) {
        this.toggleenable = true;
        this.slideState = this.slideState === 'out' ? 'in' : 'out';
        this.apiService.get_achievement(achievement_id).subscribe(
            res => {
                this.achievementdata = res;
                this.achievementdata = this.achievementdata.data;
            },
            err => console.error(err)
        );
    }

    toggleclose() {
        this.slideState = this.slideState === 'in' ? 'out' : 'in';
        this.achievementdata = '';
        this.toggleenable = false;
    }

    onEdit(achievement_id) {
        localStorage.setItem('achievement_id', achievement_id);
        this.router.navigate(['/editachievement']);
    }

    onDelete(achievement_id, is_active) {
        var data = {
            achievement_id: achievement_id,
            is_active: is_active
        };
        if (is_active == true) {
            swal({
                text: 'Are you sure?. Confirm to Activate the Achievement.',
                buttons: ['Cancel', 'Ok'],
                dangerMode: true
            })
                .then((value) => {
                    if (value) {
                        this.apiService.delete_achievement(data).subscribe((data: any) => {
                            if (data.statuscode = 204) {
                                this.list_achievements();
                                swal({
                                    text: 'Achievement Activated Successfully.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            } else {
                                swal({
                                    text: 'Something went wrong! Please try again.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            }
                        });
                    }
                });
        } else {
            swal({
                text: 'Are you sure?. Confirm to Deactivate the Achievement.',
                buttons: ['Cancel', 'Ok'],
                dangerMode: true
            })
                .then((value) => {
                    if (value) {
                        this.apiService.delete_achievement(data).subscribe((data: any) => {
                            if (data.statuscode = 204) {
                                this.list_achievements();
                                swal({
                                    text: 'Achievement Deactivated Successfully.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            } else {
                                swal({
                                    text: 'Something went wrong! Please try again.',
                                    buttons: [false],
                                    dangerMode: true,
                                    timer: 3000
                                });
                            }
                        });
                    }
                });
        }
    }

    backClicked() {
        this.location.back();
    }

}




