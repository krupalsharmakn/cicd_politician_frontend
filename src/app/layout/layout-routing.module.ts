import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { ViewcomplaintComponent } from './complaints/viewcomplaint/viewcomplaint.component';
import { MeetingrequestsComponent } from './meetingrequests/meetingrequests.component';
import { ViewmeetingrequestComponent } from './meetingrequests/viewmeetingrequest/viewmeetingrequest.component';
import { MlaprofileComponent } from './mlaprofile/mlaprofile.component';
import { EditmlaprofileComponent } from './mlaprofile/editmlaprofile/editmlaprofile.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { AddachievementComponent } from './achievements/addachievement/addachievement.component';
import { EditachievementComponent } from './achievements/editachievement/editachievement.component';
import { UserComponent } from './user/user.component';
import { NewsComponent } from './news/news.component';
import { AddnewsComponent } from './news/addnews/addnews.component';
import { EditnewsComponent } from './news/editnews/editnews.component';
import { ProjectsComponent } from './projects/projects.component';
import { ViewprojectComponent } from './projects/viewproject/viewproject.component';
import { EditprojectComponent } from './projects/editproject/editproject.component';
import { AddprojectComponent } from './projects/addproject/addproject.component';
import { ViewnewsComponent } from './news/viewnews/viewnews.component';
import { ViewuserComponent } from './user/viewuser/viewuser.component';
import { EventsComponent } from './events/events.component';
import { AddeventComponent } from './events/addevent/addevent.component';
import { EditeventComponent } from './events/editevent/editevent.component';
import { VieweventComponent } from './events/viewevent/viewevent.component';
import { BannerImagesComponent } from './banner-images/banner-images.component';
import { DepartmentComponent } from './department/department.component';
import { AdddepartmentComponent } from './department/adddepartment/adddepartment.component';
import { ViewdepartmentComponent } from './department/viewdepartment/viewdepartment.component';
import { PartyworkerComponent } from './partyworker/partyworker.component';
import { AddpartyworkerComponent } from './partyworker/addpartyworker/addpartyworker.component';
import { ViewpartyworkerComponent } from './partyworker/viewpartyworker/viewpartyworker.component';

// import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'events'
            },
            {
                path:'complaints',
                component:ComplaintsComponent
            },
            {
                path:'viewcomplaint',
                component:ViewcomplaintComponent
            },
            {
                path:'meetingrequests',
                component:MeetingrequestsComponent
            },
            {
                path:'viewmeetingrequest',
                component:ViewmeetingrequestComponent
            },
            {
                path:'mlaprofile',
                component:MlaprofileComponent
            },
            {
                path:'editmlaprofile',
                component:EditmlaprofileComponent
            },
	    {
                path:'achievements',
                component:AchievementsComponent
            },
            {
                path:'addachievement',
                component:AddachievementComponent
            },
            {
                path:'editachievement',
                component:EditachievementComponent
            },
            {
                path:'users',
                component:UserComponent
            },
            {
                path:'news',
                component:NewsComponent
            },
            {
                path:'addnews',
                component:AddnewsComponent
            },
            {
                path:'viewnews',
                component:ViewnewsComponent
            },
            {
                path: 'editnews',
                component: EditnewsComponent
            },
            {
                path: 'projects',
                component: ProjectsComponent
            },
            {
                path: 'viewproject',
                component: ViewprojectComponent
            },
            {
                path: 'editproject',
                component: EditprojectComponent
            }, 
            {
                path: 'addproject',
                component: AddprojectComponent
            },
            {
                path: 'viewuser',
                component: ViewuserComponent
            },
            {
                path: 'events',
                component: EventsComponent
            },
            {
                path: 'viewevent',
                component: VieweventComponent
            },
            {
                path: 'editevent',
                component: EditeventComponent
            },
            {
                path: 'addevent',
                component: AddeventComponent
            },
            {
                path: 'banner_images',
                component: BannerImagesComponent
            },
            {
                path: 'department',
                component: DepartmentComponent
                
            },
            {
                path: 'adddepartment',
                component: AdddepartmentComponent
                
            },
            {
                path: 'viewdepartment',
                component: ViewdepartmentComponent
                
            },
            {
                path: 'partyworker',
                component: PartyworkerComponent
                
            },
            {
                path: 'addpartyworker',
                component: AddpartyworkerComponent
                
            },
            {
                path: 'viewpartyworker',
                component: ViewpartyworkerComponent
                
            },
            
            
            // {
            //     path:'privacy_policy',
            //     component:PrivacyComponent
            // }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
