import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert';


@Component({
  selector: 'app-viewevent',
  templateUrl: './viewevent.component.html',
  styleUrls: ['./viewevent.component.css']
})
export class VieweventComponent implements OnInit {
  public eventid;
  public event_data;
  constructor(private location: Location,private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    this.eventid = localStorage.getItem('event_id')
    console.log("this.eventid = ",this.eventid)
    this.getevent();
  }

  getevent() {
    this.apiService.getevent(this.eventid).subscribe((actionArray:any) =>{
this.event_data=actionArray.data;
console.log("this.event_data = ",this.event_data)
    })
  }

  backClicked() {
    this.location.back();
  }


  onDelete(event_id, is_active) {
    var data = {
      event_id: event_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the Event.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_event(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getevent();
                            swal({
                                text: 'Event Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the Event.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_event(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getevent();
                            swal({
                                text: 'Event Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    }
  }


}
