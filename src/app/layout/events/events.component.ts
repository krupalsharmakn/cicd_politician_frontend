import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import swal from 'sweetalert';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('tableloadedvalue') table: ElementRef;
  @ViewChild(MatSort) sort: MatSort;
  displayedExcelColumns = ['sno', 'event_start_date', 'event_end_date','event_title','event_description','events_status'];
  displayedColumns = ['sno', 'event_start_date', 'event_end_date','event_title','event_description','events_status','action'];
  events: Element[];
  public datavalue = [];
  public dataSource = new MatTableDataSource(this.events);
  public excelData=new MatTableDataSource(this.events);
  filterformenable = false;
  filterform: FormGroup;


  constructor(private apiService: ApiService, private router: Router, private location: Location, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.list_events();
    this.filterform=this.formBuilder.group({
        event_start_date:['', Validators.required],
        event_end_date:['', Validators.required],

      })
  }

  list_events() {
    let allprojects = [];
    this.apiService.list_events().subscribe((data: any[]) => {
      this.dataSource = new MatTableDataSource();
      this.excelData = new MatTableDataSource();

      this.dataSource.data = data;
      this.excelData.data=data;
      console.log("PRIYANKA     ",data);
      this.datavalue = this.dataSource.data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  onopenHandled() {
    this.filterformenable = true;
  }

  events_filter(){
      console.log("events_filterdsffds");
      var filter_data={
          event_start_date:this.filterform.value.event_start_date,
          event_end_date:this.filterform.value.event_end_date
      }
      this.apiService.events_filter(filter_data).subscribe((data: any[]) => {
          console.log("dattata",data)
        this.dataSource = new MatTableDataSource();
        this.excelData = new MatTableDataSource();
        this.excelData.data=data;
        this.dataSource.data = data['data'];
        this.datavalue = this.dataSource.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }


  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'events.xlsx');
  }
  onCloseHandled() {
    this.filterformenable = false;
    this.filterform.reset()

    // this.filterform.setValue({
    //   close_status: ''
    // })

    this.filterform.setValue({
        event_start_date: '',
        event_end_date:''
    })     
    this.list_events()
    // this.table = true;
  }

  backClicked() {
    this.location.back();
  }

  onView(event_id) {
    localStorage.setItem('event_id', event_id);
    this.router.navigate(['/viewevent']);
  }

  onEdit(event_id) {
    localStorage.setItem('event_id', event_id);
    this.router.navigate(['/editevent']);
}

onDelete(event_id, is_active) {
  var data = {
    event_id: event_id,
      is_active: is_active
  };
  if (is_active == true) {
      swal({
          text: 'Are you sure?. Confirm to Activate the event.',
          buttons: ['Cancel', 'Ok'],
          dangerMode: true
      })
          .then((value) => {
              if (value) {
                  this.apiService.deactivate_event(data).subscribe((data: any) => {
                      if (data.statuscode = 204) {
                          this.list_events();
                          swal({
                              text: 'Event Activated Successfully.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 2000
                          });
                      } else {
                          swal({
                              text: 'Something went wrong! Please try again.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 2000
                          });
                      }
                  });
              }
          });
  } else {
      swal({
          text: 'Are you sure?. Confirm to Deactivate the Event.',
          buttons: ['Cancel', 'Ok'],
          dangerMode: true
      })
          .then((value) => {
              if (value) {
                  this.apiService.deactivate_event(data).subscribe((data: any) => {
                      if (data.statuscode = 204) {
                          this.list_events();
                          swal({
                              text: 'Event Deactivated Successfully.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 2000
                          });
                      } else {
                          swal({
                              text: 'Something went wrong! Please try again.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 2000
                          });
                      }
                  });
              }
          });
  }
}

}
