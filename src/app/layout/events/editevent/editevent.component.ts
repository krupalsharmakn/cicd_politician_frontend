import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import swal from 'sweetalert';
import { Location } from '@angular/common';

@Component({
  selector: 'app-editevent',
  templateUrl: './editevent.component.html',
  styleUrls: ['./editevent.component.css']
})
export class EditeventComponent implements OnInit {

  public event_id;
public updateForm: FormGroup;
public loader = false;
public event_data;
public uploadimage;
public eventimage;
public submitted = false;
public isActive;
public details;
public afterchange=false;
myFiles: string[] = [];


  constructor(private location: Location, private formBuilder: FormBuilder, private apiService: ApiService, private router: Router, public translate: TranslateService) { }

  ngOnInit() {
    this.event_id = localStorage.getItem('event_id')
    this.getevent();

    this.updateForm = this.formBuilder.group({
      event_title: ['', Validators.required],
      event_description: ['', Validators.required],
      // project_status: ['', Validators.required],
      event_start_date: ['',Validators.required],
      event_end_date: [''],
      is_active: [''],
      contact_person_name:[''],
      contact_person_num:[''],
      taluk: [''],
      district: [''],
    });
  }

  get f() { return this.updateForm.controls; }


  getevent() {
    this.loader = true;

    this.apiService.getevent(this.event_id).subscribe((actionArray: any) => {
      this.event_data = actionArray
this.details = this.event_data.data;
console.log("this.details = ",this.details.event_id,this.details.is_active)
      this.loader = false;
      if (this.event_data.statuscode == 200) {
        var eventdata = this.event_data.data;
        // this.uploadimage = eventdata.event_images || '';
        console.log("eventdata.project_start_date",eventdata.event_start_date)

        var d1 = new Date(eventdata.event_start_date),
        month = '' + (d1.getMonth() + 1),
        day = '' + d1.getDate(),
        year = d1.getFullYear();
        var start_date = day+'-'+month+'-'+year;
        console.log("start_date",start_date)
        var d2 = new Date(eventdata.event_end_date),
        month = '' + (d2.getMonth() + 1),
        day = '' + d2.getDate(),
        year = d2.getFullYear();
        console.log("d1",d1)
        var end_date = day+'-'+month+'-'+year;
        this.uploadimage = eventdata.event_images || '';

        this.myFiles = eventdata.event_images;
                    for(let i=0;i<eventdata.event_images.length;i++)
                     this.urls.push(eventdata.event_images[i]); 
                    console.log("newsdatas",this.urls)
        this.updateForm.setValue({
          event_title: eventdata.event_title || '',
          event_description: eventdata.event_description || '',
          event_start_date: eventdata.event_start_date || '',
          event_end_date: eventdata.event_end_date || '',
          taluk:eventdata.taluk || '',
          district:eventdata.district || '',
          is_active: eventdata.is_active || '',
          
      contact_person_name:eventdata.contact_person_name || '',
      contact_person_num:eventdata.contact_person_num || ''
        });
    }
    console.log("this.updateForm6,this.updateForm",this.updateForm)
   
    }, err => console.error(err)
    )
  }
  urls = [];
  urldata = []
  getFileDetails(e) {
    this.afterchange=true;
    console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);

      var reader = new FileReader();

      reader.onload = (event: any) => {
        console.log(event.target.result);
        this.urldata.push(event.target.result);
      }

      reader.readAsDataURL(e.target.files[i]);
    }
  }
  fileChange(e) {
    const photos = e.target.files[0];
    this.eventimage = photos;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(photos);
  }
  imagepath
  images = new Array();

  onSubmit() {

    
      this.imagepath = ''
      this.images = []
      const frmData = new FormData();
      for (var i = 0; i < this.myFiles.length; i++) {
        this.imagepath = this.myFiles[i];
        console.log(this.imagepath);
        this.images.push(this.imagepath)
       
      }
    const formData: FormData = new FormData();
    console.log("event_start_date = ",this.updateForm.value.event_start_date)
    var d1 = new Date(this.updateForm.value.event_start_date),
    month = '' + (d1.getMonth() + 1),
    day = '' + d1.getDate(),
    year = d1.getFullYear();
    var start_date1 = day+'-'+month+'-'+year;
    console.log("start_date1",start_date1)
    console.log("new date = ",new Date(this.updateForm.value.event_start_date))
    // const file: File = this.eventimage;
      formData.append('event_title', this.updateForm.value.event_title);
      formData.append('event_description', this.updateForm.value.event_description);
      formData.append('event_start_date', this.updateForm.value.event_start_date);
      formData.append('event_end_date', this.updateForm.value.event_end_date);
      formData.append('contact_person_name', this.updateForm.value.contact_person_name);

      formData.append('contact_person_num', this.updateForm.value.contact_person_num);
      formData.append('taluk', this.updateForm.value.taluk);
      formData.append('district', this.updateForm.value.district);

    formData.append('event_id', this.event_id);

    for (i = 0; i < this.images.length; i++) {
      var file: File = this.images[i];
      formData.append('image', file);
    }
    console.log("file",file)
    // formData.append('image', file);
    this.submitted = true;
    console.log("this.updateForm",this.updateForm.value)
    console.log("this.updateForm",this.updateForm.valid)

    if (this.updateForm.valid) {
      this.apiService.edit_event(formData).subscribe((data: any) => {
        console.log("data",data)
        if (data.statuscode = 200) {
          swal({
            text: "Event details updated Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 2000
          });
          this.router.navigate(['/viewevent']);
        } else {
          swal({
            text: "Failed to Update event details.",
            buttons: [false],
            dangerMode: true,
            timer: 2000
          });
        }
      });
    }
  }

  backClicked() {
    this.location.back();
  }

  onDelete(event_id, is_active) {
    var data = {
      event_id: event_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the event.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_event(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getevent();
                            swal({
                                text: 'Event Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the Event.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_event(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getevent();
                            swal({
                                text: 'Event Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    }
  }


}
