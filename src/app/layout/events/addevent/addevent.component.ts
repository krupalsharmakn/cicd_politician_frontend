import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';

@Component({
  selector: 'app-addevent',
  templateUrl: './addevent.component.html',
  styleUrls: ['./addevent.component.css']
})
export class AddeventComponent implements OnInit {
  public eventimage;
  public uploadimage;
  public loader = false;
  submitted = false;
  public addevent: FormGroup;
  myFiles: string[] = [];
  constructor(private location: Location,private apiService: ApiService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    const file: File = this.eventimage;
    this.addevent = this.formBuilder.group({
      event_title: ['', Validators.required],
      event_description: ['', Validators.required],
      event_start_date: ['', Validators.required],
      event_end_date: [''],
      image: [''],
      contact_person_name: [''],
      taluk: [''],
      district: [''],
      contact_person_num: ['',[Validators.required, Validators.pattern("[6-9]\\d{9}")]],


    });
  }

  get f() { return this.addevent.controls; }
  imagepath
  images = new Array();
  onSubmit() {
    this.imagepath = ''
    this.images = []
    const frmData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      this.imagepath = this.myFiles[i];
      console.log(this.imagepath);
      this.images.push(this.imagepath)
     
    }
    const event_data: FormData = new FormData();
    // const file: File = this.eventimage;
    // event_data.append('event_id', this.addevent.value.event_id);
    event_data.append('event_title', this.addevent.value.event_title);
    event_data.append('event_description', this.addevent.value.event_description);
    event_data.append('event_start_date', this.addevent.value.event_start_date);
    event_data.append('event_end_date', this.addevent.value.event_end_date);
    event_data.append('contact_person_name', this.addevent.value.contact_person_name);
    event_data.append('contact_person_num', this.addevent.value.contact_person_num);
    event_data.append('taluk', this.addevent.value.taluk);
    event_data.append('district', this.addevent.value.district);

    for (i = 0; i < this.images.length; i++) {
      var file: File = this.images[i];
      event_data.append('image', file);
    }
    // event_data.append('image', file);
    this.submitted = true;
   
    if (this.addevent.valid) {
      this.loader = false;
      this.apiService.add_event(event_data).subscribe((data: any) => {
        if (data.statuscode == 200) {
          swal({
            text: "Event added Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 2000
          });
          this.router.navigate(['/events']);
          this.addevent.reset();
        } else {
          swal({
            text: "Something went wrong! Please try again.",
            buttons: [false],
            dangerMode: true,
            timer: 2000
          });
        }
      });
    } else {
      swal({
        text: "Please fill all the details",
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }

  fileChange(e) {
    const profiles = e.target.files[0];
    this.eventimage = profiles;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(profiles);
  }

  backClicked() {
    this.location.back();
  }
  urls = [];

  getFileDetails(e) {
    console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);

      var reader = new FileReader();

      reader.onload = (event: any) => {
        console.log(event.target.result);
        this.urls.push(event.target.result);
      }

      reader.readAsDataURL(e.target.files[i]);
    }
  }

}
