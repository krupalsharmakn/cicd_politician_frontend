import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { Router } from '@angular/router';
import { NavigationExtras } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert';
import { DatePipe } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
@Component({
  selector: 'app-meetingrequests',
  templateUrl: './meetingrequests.component.html',
  styleUrls: ['./meetingrequests.component.css']
})
export class MeetingrequestsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('tableloadedvalue') table: ElementRef;
  @ViewChild('tablefilterloadedvalue') tablefilter: ElementRef;

  filterform: FormGroup;
  public exceldata :any;
public filterdataSource:any;
  public dataSource: any;
  public meeting_list;
  toggleForm: boolean = false;
  datatable: boolean = true;
  filterformenable = false;
  filtertable: boolean = false;


  displayedexcelColumns = ['sno','subject','day_of_meeting','reason_for_meet','address'];

  displayedColumns = ['sno','subject','day_of_meeting','reason_for_meet','actions'];

  constructor(private apiService: ApiService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.list_meetings();
    this.filterform = this.formBuilder.group({
      is_active: ['', Validators.required],
      meeting_start_date:['', Validators.required],
      meeting_end_date:['', Validators.required],
      // created_start_date:['', Validators.required],
      // created_end_date:['', Validators.required]

    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Meetings.xlsx');
  }


  exceldatafilterimport() {
    console.log("dffsdfsff",this.tablefilter.nativeElement)
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tablefilter.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Meetings.xlsx');
  }
  list_meetings() {
    this.apiService.list_meetings().subscribe((data: any[]) => {
      this.dataSource = new MatTableDataSource();
      this.exceldata= new MatTableDataSource();
      this.dataSource.data = data;
      this.exceldata.data=data;
      console.log("this.dataSource.data",this.dataSource.data)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }



  meetingfilter(){
    if(this.filterform.value.meeting_end_date.length==0){
      this.filterform.value.meeting_end_date=new Date()
    }
    var filter_data={
      is_active:this.filterform.value.is_active,
      meeting_start_date:this.filterform.value.meeting_start_date,
      meeting_end_date:this.filterform.value.meeting_end_date

    }
console.log("filter_data",filter_data);
this.datatable = false;
this.filtertable = true;

    this.apiService.meeting_filter(filter_data).subscribe((data: any[]) => {
      this.dataSource = new MatTableDataSource()
      var filterdata=data['data'];
      this.filterdataSource= new MatTableDataSource();
      console.log("dataaaaa",data)
      this.dataSource.data = filterdata
      this.filterdataSource.data=filterdata;
      console.log("this.filterdataSource",this.filterdataSource)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  onView(meeting_request_id) {
    localStorage.setItem('meeting_request_id', meeting_request_id);
    this.router.navigate(['/viewmeetingrequest']);
  }
  onopenHandled() {
    this.filterformenable = true;
    // this.table = false;
  }
  onCloseHandled() {
    this.filterformenable = false;
    this.filterform.reset()
    this.datatable = true;
    this.filtertable = false;

    // this.filterform.setValue({
    //   close_status: ''
    // })

    this.filterform.setValue({
        is_active: '',
        meeting_start_date:'',
        meeting_end_date:'',
    
    })     
    this.list_meetings()
    // this.table = true;
  }

  reject(meeting_request_id, is_active) {
    if (is_active == true) {
      swal({
        text: "Are you sure that you want to accept the meeting request ?",
        buttons: ['Cancel', 'Ok'],
        dangerMode: true,
      })
        .then((value) => {
          if (value) {
            this.apiService.deactivate_meeting_request(meeting_request_id, is_active).subscribe((data: any) => {
              if (data.statuscode = 200) {
                swal({
                  text: "Meeting request successful.",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              } else {
                swal({
                  text: "Failed to accept meeting request",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              }
            });
          }
        });
    } else {
      if(is_active == false){
        swal({
          text: "Are you sure you want to reject the meeting request ?",
          buttons: ['Cancel', 'Ok'],
          dangerMode: true,
        })
          .then((value) => {
            if (value) {
              this.apiService.deactivate_meeting_request(meeting_request_id, is_active).subscribe((data: any) => {
                if (data.statuscode = 200) {
                  swal({
                    text: "meeting request rejected successfully",
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                } else {
                  swal({
                    text: "Failed to reject meeting request",
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                }
              });
            }
          });
      }
    }
  }
}
