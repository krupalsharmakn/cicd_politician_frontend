import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmeetingrequestComponent } from './viewmeetingrequest.component';

describe('ViewmeetingrequestComponent', () => {
  let component: ViewmeetingrequestComponent;
  let fixture: ComponentFixture<ViewmeetingrequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewmeetingrequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmeetingrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
