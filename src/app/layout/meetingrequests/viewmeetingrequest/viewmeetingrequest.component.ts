import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-viewmeetingrequest',
  templateUrl: './viewmeetingrequest.component.html',
  styleUrls: ['./viewmeetingrequest.component.css']
})
export class ViewmeetingrequestComponent implements OnInit {
  public meetingrequestid;
  public meeting_list;
  public viewcomplaint;
  public actionArraydata;
  list;
  constructor(private location: Location,private apiService: ApiService, private router: Router) { }

  ngOnInit() {
    this.meetingrequestid = localStorage.getItem('meeting_request_id')
    this.getmeetingrequest();
  }


  getmeetingrequest() {
    this.apiService.getmeetingrequest(this.meetingrequestid).subscribe((actionArray:any) =>{   
this.meeting_list=actionArray.data
    })
  }

  backClicked() {
    this.location.back();
  }

}
