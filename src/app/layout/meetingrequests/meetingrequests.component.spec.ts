import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeetingrequestsComponent } from './meetingrequests.component';

describe('MeetingrequestsComponent', () => {
  let component: MeetingrequestsComponent;
  let fixture: ComponentFixture<MeetingrequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeetingrequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeetingrequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
