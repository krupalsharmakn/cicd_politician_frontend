import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavComponent } from './nav/nav.component';
import { PageHeaderComponent } from '../shared/modules/page-header/page-header.component';
import { MaterialModule } from '../shared/modules/material/material-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { StatModule } from '../shared/modules/stat/stat.module';
import { TranslateModule } from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ComplaintsComponent } from './complaints/complaints.component';
import { ViewcomplaintComponent } from './complaints/viewcomplaint/viewcomplaint.component';
import { MeetingrequestsComponent } from './meetingrequests/meetingrequests.component';
import { ViewmeetingrequestComponent } from './meetingrequests/viewmeetingrequest/viewmeetingrequest.component';
import { MlaprofileComponent } from './mlaprofile/mlaprofile.component';
import { EditmlaprofileComponent } from './mlaprofile/editmlaprofile/editmlaprofile.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { AddachievementComponent } from './achievements/addachievement/addachievement.component';
import { EditachievementComponent } from './achievements/editachievement/editachievement.component';
import { UserComponent } from './user/user.component';
import { NewsComponent } from './news/news.component';
import { AddnewsComponent } from './news/addnews/addnews.component';
import { EditnewsComponent } from './news/editnews/editnews.component';
import { ProjectsComponent } from './projects/projects.component';
import { ViewprojectComponent } from './projects/viewproject/viewproject.component';
import { EditprojectComponent } from './projects/editproject/editproject.component';
import { AddprojectComponent } from './projects/addproject/addproject.component';
import { ViewnewsComponent } from './news/viewnews/viewnews.component';
import { ViewuserComponent } from './user/viewuser/viewuser.component';
import { EventsComponent } from './events/events.component';
import { AddeventComponent } from './events/addevent/addevent.component';
import { EditeventComponent } from './events/editevent/editevent.component';
import { VieweventComponent } from './events/viewevent/viewevent.component';
import { AgmCoreModule } from '@agm/core';
import{ DialogNotification} from './user/user.component';
import {DialogAlluserNotification}  from './user/user.component';
import { BannerImagesComponent } from './banner-images/banner-images.component';
import { DepartmentComponent } from './department/department.component';
import { AdddepartmentComponent } from './department/adddepartment/adddepartment.component';
import { ViewdepartmentComponent } from './department/viewdepartment/viewdepartment.component';
import { PartyworkerComponent } from './partyworker/partyworker.component';
import { AddpartyworkerComponent } from './partyworker/addpartyworker/addpartyworker.component';
import { ViewpartyworkerComponent } from './partyworker/viewpartyworker/viewpartyworker.component';
// import { PrivacyComponent } from './privacy/privacy.component';
@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MaterialModule,
        StatModule,
        FormsModule, ReactiveFormsModule,
        TranslateModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyB-i-geNyumo0S8t3i9NWnMK0bae5qV4xo'
            }),
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [
        LayoutComponent,
        NavComponent,
        TopnavComponent,
        SidebarComponent,
        PageHeaderComponent,
        ComplaintsComponent,
        ViewcomplaintComponent,
        MeetingrequestsComponent,
        ViewmeetingrequestComponent,
        MlaprofileComponent,
        EditmlaprofileComponent,
        AchievementsComponent,
        AddachievementComponent,
        EditachievementComponent,
        UserComponent,
        NewsComponent,
        AddnewsComponent,
        EditnewsComponent,
        ProjectsComponent,
        ViewprojectComponent,
        EditprojectComponent,
        AddprojectComponent,
        ViewnewsComponent,
        ViewuserComponent,
        EventsComponent,
        AddeventComponent,
        EditeventComponent,
        DialogNotification,
        DialogAlluserNotification,
        VieweventComponent,
        BannerImagesComponent,
        DepartmentComponent,
        AdddepartmentComponent,
        ViewdepartmentComponent,
        PartyworkerComponent,
        AddpartyworkerComponent,
        ViewpartyworkerComponent,
        // PrivacyComponent
    ],
        entryComponents: [
            DialogNotification,DialogAlluserNotification
          ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class LayoutModule {}
