import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MlaprofileComponent } from './mlaprofile.component';

describe('MlaprofileComponent', () => {
  let component: MlaprofileComponent;
  let fixture: ComponentFixture<MlaprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MlaprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlaprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
