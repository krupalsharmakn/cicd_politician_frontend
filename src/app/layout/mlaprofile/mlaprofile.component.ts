import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import swal from 'sweetalert';
@Component({
  selector: 'app-mlaprofile',
  templateUrl: './mlaprofile.component.html',
  styleUrls: ['./mlaprofile.component.css']
})
export class MlaprofileComponent implements OnInit {
  public mla_id;
  public mla_profile;
  public mla_profile_tamil;
  public  language='en';
  currentLang
  constructor(private apiService: ApiService, private router: Router, public translate: TranslateService) {
    this.currentLang = translate.currentLang;

    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang;
      console.log(this.currentLang);
      this.language = this.currentLang;
    });
   }
  
 
  ngOnInit() {
    this.mla_id = localStorage.getItem('mla_id')
    // this.language = localStorage.getItem('language') || 'en';
    // console.log("this.language  = ",this.language)

  

  this.getmlaprofile()


  }

  

  getmlaprofile() {
    this.apiService.get_mla_profile(this.mla_id).subscribe((actionArray:any) =>{   
this.mla_profile= actionArray.data;
console.log("this.mla_profile = ",this.mla_profile)
    })
  }

//   getmlaprofile_tamil() {
//     this.apiService.get_mla_profile(this.mla_id).subscribe((actionArray:any) =>{   
// this.mla_profile_tamil= actionArray.data;
// console.log("this.mla_profile_tamil = ",this.mla_profile_tamil)

//     })
//   }

  onedit(mla_profile_id) {
    localStorage.setItem('mla_profile_id', mla_profile_id);
    this.router.navigate(['/editmlaprofile']);
  }
}
