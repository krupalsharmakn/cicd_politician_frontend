import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditmlaprofileComponent } from './editmlaprofile.component';

describe('EditmlaprofileComponent', () => {
  let component: EditmlaprofileComponent;
  let fixture: ComponentFixture<EditmlaprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditmlaprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditmlaprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
