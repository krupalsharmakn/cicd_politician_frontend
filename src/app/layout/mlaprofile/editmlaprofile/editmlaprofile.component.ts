import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import swal from 'sweetalert';
import { Location } from '@angular/common';




@Component({
  selector: 'app-editmlaprofile',
  templateUrl: './editmlaprofile.component.html',
  styleUrls: ['./editmlaprofile.component.css']
})
export class EditmlaprofileComponent implements OnInit {
  public mla_id;
  public mla_profile;
  public profile;
  public uploadimage;
  public image;
  public profileimage;
  updateForm: FormGroup;
  public currentuser;
  loader = false;
  submitted = false;
  public  language='en';
  currentLang;

  constructor(private location: Location, private formBuilder: FormBuilder, private apiService: ApiService, private router: Router, public translate: TranslateService) {
    translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.currentLang = event.lang;
      console.log(this.currentLang);
      // if()
      localStorage.getItem("language")
      this.language = this.currentLang;
      this.getmlaprofile()
    });
   }

  ngOnInit() {
    this.mla_id = localStorage.getItem('mla_id')
    this.getmlaprofile()
    
    this.updateForm = this.formBuilder.group({
      mla_name: ['', Validators.required],
      party_name: ['', Validators.required],
      constituency_name: ['', Validators.required],
      profile_description: [''],
      address: [''],
      twitter_id: [''],
      facebook_id: [''],
      linkedin_id: [''],
      mail_id: [''],
      public_meeting_timings: [''],
    });

  }

  get f() { return this.updateForm.controls; }

  getmlaprofile() {
    this.loader = true;

    this.apiService.get_mla_profile(this.mla_id).subscribe((actionArray: any) => {
      this.mla_profile = actionArray
      this.loader = false;
      if (this.mla_profile.statuscode == 200) {
        const mlaprofiledata = this.mla_profile.data;
        this.uploadimage = mlaprofiledata.profile_image || '';
        if(this.currentLang=='tl')
        {
          console.log("this.language",this.currentLang,mlaprofiledata);
        this.updateForm.setValue({
          mla_name: mlaprofiledata.mla_name_t || '',
          party_name: mlaprofiledata.party_name_t || '',
          constituency_name: mlaprofiledata.constituency_name_t || '',
          address: mlaprofiledata.address_t || '',
          facebook_id: mlaprofiledata.facebook_id || '',
          linkedin_id: mlaprofiledata.linkedin_id || '',
          mail_id: mlaprofiledata.mail_id || '',
          profile_description: mlaprofiledata.profile_description_t || '',
          public_meeting_timings: mlaprofiledata.public_meeting_timings || '',
          twitter_id: mlaprofiledata.twitter_id || '',
        });
      }
      else{
        this.updateForm.setValue({
          mla_name: mlaprofiledata.mla_name || '',
          party_name: mlaprofiledata.party_name || '',
          constituency_name: mlaprofiledata.constituency_name || '',
          address: mlaprofiledata.address || '',
          facebook_id: mlaprofiledata.facebook_id || '',
          linkedin_id: mlaprofiledata.linkedin_id || '',
          mail_id: mlaprofiledata.mail_id || '',
          profile_description: mlaprofiledata.profile_description || '',
          public_meeting_timings: mlaprofiledata.public_meeting_timings || '',
          twitter_id: mlaprofiledata.twitter_id || '',
        });
      }
    }
   
    }, err => console.error(err)
    )
  }

  fileChange(e) {
    const profiles = e.target.files[0];
    this.profile = profiles;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(profiles);
  }

  onSubmit() {
    const formData: FormData = new FormData();
    const file: File = this.profile;
  console.log(this.updateForm.value);
  
    if(this.currentLang=='tl')
    {
      formData.append('mla_name_t', this.updateForm.value.mla_name);
    formData.append('party_name_t', this.updateForm.value.party_name);
    formData.append('constituency_name_t', this.updateForm.value.constituency_name);
    formData.append('profile_description_t', this.updateForm.value.profile_description);
    formData.append('address_t', this.updateForm.value.address);
   
    }
    else{
      formData.append('mla_name', this.updateForm.value.mla_name);
      formData.append('party_name', this.updateForm.value.party_name);
      formData.append('constituency_name', this.updateForm.value.constituency_name);
      formData.append('profile_description', this.updateForm.value.profile_description);
      formData.append('address', this.updateForm.value.address);
    }
    formData.append('mla_profile_id', this.mla_id);
    formData.append('twitter_id', this.updateForm.value.twitter_id);
    formData.append('facebook_id', this.updateForm.value.facebook_id);
    formData.append('linkedin_id', this.updateForm.value.linkedin_id);
    formData.append('mail_id', this.updateForm.value.mail_id);
    formData.append('public_meeting_timings', this.updateForm.value.public_meeting_timings);
    formData.append('image', file);

    this.submitted = true;
    if (this.updateForm.valid) {
      this.apiService.edit_mla_profile(formData).subscribe((data: any) => {
        if (data.statuscode = 200) {
          swal({
            text: "Profile Updated Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
          this.router.navigate(['/mlaprofile']);
        } else {
          swal({
            text: "Failed to Update profile.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    }
  }

  backClicked() {
    this.location.back();
  }

}

