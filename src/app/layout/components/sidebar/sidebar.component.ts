import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public showMenu: string;
    constructor(private router: Router) {}

    ngOnInit() {
        this.showMenu = '';
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    viewprofile(){
        var mla_id = "d8f6a9c2-1e22-4321-8b2a-4d5a156b17e8"
        localStorage.setItem('mla_id', mla_id);
        this.router.navigate(['/mlaprofile']);
    }
}
