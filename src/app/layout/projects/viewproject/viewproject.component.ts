import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert';


@Component({
  selector: 'app-viewproject',
  templateUrl: './viewproject.component.html',
  styleUrls: ['./viewproject.component.css']
})
export class ViewprojectComponent implements OnInit {
  public projectid;
  public project_data;
  constructor(private location: Location,private apiService: ApiService, private router: Router) { }
  

  ngOnInit() {

    this.projectid = localStorage.getItem('project_id')
    console.log("this.projectid = ",this.projectid)
    this.getproject();
  }


  getproject() {
    this.apiService.getproject(this.projectid).subscribe((actionArray:any) =>{
this.project_data=actionArray.data;
console.log("this.project_data = ",this.project_data)
    })
  }

  backClicked() {
    this.location.back();
  }


  onDelete(project_id, is_active) {
    var data = {
      project_id: project_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the project.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_project(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getproject();
                            swal({
                                text: 'Project Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the Project.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_project(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getproject();
                            swal({
                                text: 'Project Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    }
  }
  
}
