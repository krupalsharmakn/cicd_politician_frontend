import { Component, OnInit ,ViewChild,ElementRef} from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { Router } from '@angular/router';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormGroup, FormBuilder, Validators,} from '@angular/forms';
import { Location } from '@angular/common';
import swal from 'sweetalert';
import * as XLSX from 'xlsx';

export interface projectlist {
    value: string;
    viewValue: string;
  }
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('tableloadedvalue') table: ElementRef;

  displayedExcelColumns = ['sno', 'project_start_date', 'project_end_date','project_title','project_description','project_status'];
  displayedColumns = ['sno', 'project_start_date', 'project_end_date','project_title','project_description','project_status','action'];
  products: Element[];
  project_status_list: projectlist[];

  public dataSource = new MatTableDataSource(this.products);
  public excelData= new MatTableDataSource(this.products);
  public datavalue = [];
  filterform: FormGroup;
  filterformenable = false;


  constructor(private apiService: ApiService, private router: Router,private location: Location, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.list_projects();
    this.project_status_list = [

        { value: 'completed', viewValue: 'Completed' },
        { value: 'ongoing', viewValue: 'Ongoing' }
     
  
      ]
    this.filterform = this.formBuilder.group({
        is_active:['', Validators.required],
        project_status: ['', Validators.required],
        project_start_date:['', Validators.required],
        project_end_date:['', Validators.required],
        // created_start_date:['', Validators.required],
        // created_end_date:['', Validators.required]
  
      });
  }

  list_projects() {
    let allprojects = [];
    this.apiService.list_projects().subscribe((data: any[]) => {
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = data;
      this.excelData.data=data;
      console.log("PRIYANKA     ",data);
      this.datavalue = this.dataSource.data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;


    });

  }
  onopenHandled() {
    this.filterformenable = true;
    // this.table = false;
  }

  backClicked() {
    this.location.back();
  }

  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'projectlist.xlsx');
  }

  projectfilter(){
      console.log("this.filterform.value",this.filterform.value.project_start_date);
      console.log("new dateee",new Date(this.filterform.value.project_start_date));
      var is_active;
      if(this.filterform.value.is_active.length==0){
        is_active="true";
      }
      else{
        is_active=this.filterform.value.is_active
      }
      if(this.filterform.value.project_end_date.length==0){
        this.filterform.value.project_end_date=new Date()
      }
      var filter_data={
        is_active:is_active,
        project_status:this.filterform.value.project_status,
        project_start_date:this.filterform.value.project_start_date,
        project_end_date:this.filterform.value.project_end_date
      }
      console.log("dataaaaa",filter_data)
    this.apiService.projects_filter(filter_data).subscribe((data: any[]) => {
        console.log("PRIYANKA     ",data);

        this.dataSource = new MatTableDataSource();
        this.dataSource.data = data['data'];
        this.excelData.data= data['data']
        this.datavalue = this.dataSource.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
  
  
      });
}
  onView(project_id) {
    localStorage.setItem('project_id', project_id);
    this.router.navigate(['/viewproject']);
  }


  onEdit(project_id) {
    localStorage.setItem('project_id', project_id);
    this.router.navigate(['/editproject']);
}
onCloseHandled() {
    this.filterformenable = false;
    this.filterform.reset()

    // this.filterform.setValue({
    //   close_status: ''
    // })

    this.filterform.setValue({
        is_active: '',
        project_status:'',
        project_start_date:'',
        project_end_date:'',
    
    })     
    this.list_projects()
    // this.table = true;
  }
onDelete(project_id, is_active) {
  var data = {
    project_id: project_id,
      is_active: is_active
  };
  if (is_active == true) {
      swal({
          text: 'Are you sure?. Confirm to Activate the project.',
          buttons: ['Cancel', 'Ok'],
          dangerMode: true
      })
          .then((value) => {
              if (value) {
                  this.apiService.deactivate_project(data).subscribe((data: any) => {
                      if (data.statuscode = 204) {
                          this.list_projects();
                          swal({
                              text: 'Project Activated Successfully.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 3000
                          });
                      } else {
                          swal({
                              text: 'Something went wrong! Please try again.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 3000
                          });
                      }
                  });
              }
          });
  } else {
      swal({
          text: 'Are you sure?. Confirm to Deactivate the Project.',
          buttons: ['Cancel', 'Ok'],
          dangerMode: true
      })
          .then((value) => {
              if (value) {
                  this.apiService.deactivate_project(data).subscribe((data: any) => {
                      if (data.statuscode = 204) {
                          this.list_projects();
                          swal({
                              text: 'Project Deactivated Successfully.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 3000
                          });
                      } else {
                          swal({
                              text: 'Something went wrong! Please try again.',
                              buttons: [false],
                              dangerMode: true,
                              timer: 3000
                          });
                      }
                  });
              }
          });
  }
}

}
