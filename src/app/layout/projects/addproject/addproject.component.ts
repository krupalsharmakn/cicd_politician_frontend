import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
@Component({
  selector: 'app-addproject',
  templateUrl: './addproject.component.html',
  styleUrls: ['./addproject.component.css']
})
export class AddprojectComponent implements OnInit {
  public projectimage;
  public uploadimage;
  loader = false;
  submitted = false;
  addproject: FormGroup;
  myFiles: string[] = [];

  constructor(private location: Location,private apiService: ApiService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {
    const file: File = this.projectimage;
    this.addproject = this.formBuilder.group({
      project_title: ['', Validators.required],
      project_description: ['', Validators.required],
      project_start_date: ['', Validators.required],
      project_end_date: ['',Validators.required],
      project_status:['', Validators.required],
      image: ['']
    });
  }
  get f() { return this.addproject.controls; }
  imagepath
  images = new Array();
  
    
  onSubmit() {
    this.imagepath = ''
    this.images = []
    const frmData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      this.imagepath = this.myFiles[i];
      console.log(this.imagepath);
      this.images.push(this.imagepath)
     
    }
    const project_data: FormData = new FormData();
    // const file: File = this.projectimage;
    project_data.append('project_id', this.addproject.value.achievement_id);
    project_data.append('project_title', this.addproject.value.project_title);
    project_data.append('project_description', this.addproject.value.project_description);
    project_data.append('project_start_date', this.addproject.value.project_start_date);
    project_data.append('project_end_date', this.addproject.value.project_end_date);
    project_data.append('project_status', this.addproject.value.project_status);
    // project_data.append('image', file);
    for (i = 0; i < this.images.length; i++) {
      var file: File = this.images[i];
      project_data.append('image', file);
    }
    this.submitted = true;
    console.log("this.addproject.value.project_start_date = ",this.addproject.value.project_start_date)
    console.log("project_data.project_start_date = ",project_data)
   
    if (this.addproject.valid) {
      this.loader = false;
      this.apiService.add_project(project_data).subscribe((data: any) => {
        if (data.statuscode == 200) {
          swal({
            text: "Project added Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
          this.router.navigate(['/projects']);
          this.addproject.reset();
        } else {
          swal({
            text: "Something went wrong! Please try again.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    } else {
      swal({
        text: "Please fill all the details",
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }

  
  fileChange(e) {
    const profiles = e.target.files[0];
    this.projectimage = profiles;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(profiles);
  }

  backClicked() {
    this.location.back();
  }

  urls=[];
  getFileDetails(e) {
    console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);

      var reader = new FileReader();

      reader.onload = (event: any) => {
        console.log(event.target.result);
        this.urls.push(event.target.result);
      }

      reader.readAsDataURL(e.target.files[i]);
    }
  }
}
