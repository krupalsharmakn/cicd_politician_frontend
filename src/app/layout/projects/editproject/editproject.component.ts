import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import swal from 'sweetalert';
import { Location } from '@angular/common';
export interface projectlist {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-editproject',
  templateUrl: './editproject.component.html',
  styleUrls: ['./editproject.component.css']
})
export class EditprojectComponent implements OnInit {
public project_id;
public updateForm: FormGroup;
public loader = false;
public project_data;
public uploadimage;
public profileimage;
public submitted = false;
public isActive;
public details;
public date_start_date;
public event_change;
public afterchange=false;
myFiles: string[] = [];

project_status_list: projectlist[];


  constructor(private location: Location, private formBuilder: FormBuilder, private apiService: ApiService, private router: Router, public translate: TranslateService) { }

  ngOnInit() {
    this.project_id = localStorage.getItem('project_id')
    this.getproject()
    this.project_status_list = [
      { value: 'completed', viewValue: 'Completed' },
      { value: 'ongoing', viewValue: 'Ongoing' }
    ]

    this.updateForm = this.formBuilder.group({
      project_title: ['', Validators.required],
      project_description: ['', Validators.required],
      project_status: ['', Validators.required],
      project_start_date: ['',Validators.required],
      project_end_date: ['',Validators.required],
      is_active: ['']
    });
  }

  get f() { return this.updateForm.controls; }

  getproject() {
    this.loader = true;

    this.apiService.getproject(this.project_id).subscribe((actionArray: any) => {
      this.project_data = actionArray
this.details = this.project_data.data;
      this.loader = false;
      if (this.project_data.statuscode == 200) {
        var projectdata = this.project_data.data;
        console.log("this.details = ",this.details)

        // this.uploadimage = projectdata.project_images || '';
        var d1 = new Date(projectdata.project_start_date),
        month = '' + (d1.getMonth() + 1),
        day = '' + d1.getDate(),
        year = d1.getFullYear();
        var start_date = day+'-'+month+'-'+year;
        var d2 = new Date(projectdata.project_end_date),
        month = '' + (d2.getMonth() + 1),
        day = '' + d2.getDate(),
        year = d2.getFullYear();
        var end_date = day+'-'+month+'-'+year;
        this.date_start_date=projectdata.start_date;
        this.uploadimage = projectdata.project_images || '';

        this.myFiles = projectdata.project_images;
                    for(let i=0;i<projectdata.project_images.length;i++)
                     this.urls.push(projectdata.project_images[i]); 
        this.updateForm.setValue({
          project_title: projectdata.project_title || '',
          project_description: projectdata.project_description || '',
          project_start_date: projectdata.project_start_date || '',
          project_end_date:projectdata.project_end_date || '',
          project_status: projectdata.project_status || '',
          is_active: projectdata.is_active || ''
        });
    }
   console.log("this.updateForm",this.updateForm)
    }, err => console.error(err)
    )
  }
  urls = [];
  urldata = []
  getFileDetails(e) {
    this.afterchange=true;
    console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);

      var reader = new FileReader();

      reader.onload = (event: any) => {
        console.log(event.target.result);
        this.urldata.push(event.target.result);
      }

      reader.readAsDataURL(e.target.files[i]);
    }
  }
  fileChange(e) {
    const photos = e.target.files[0];
    this.profileimage = photos;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(photos);
  }
  status_changes(event){
console.log("event",event)
var selectElement = event.target;
var value = selectElement.value;
this.event_change=value;
console.log("this.event_change",this.event_change)
  }


  imagepath
  images = new Array();
  onSubmit() {
    console.log("sddddd", this.event_change)
    var status;
    this.imagepath = ''
    this.images = []
    const frmData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      this.imagepath = this.myFiles[i];
      console.log(this.imagepath);
      this.images.push(this.imagepath)
     
    }
    if(this.event_change==undefined){
      status=this.updateForm.value.project_status;
    }else{
      status=this.event_change
    }
    console.log("status",status)
    const formData: FormData = new FormData();
    // const file: File = this.profileimage;
      formData.append('project_title', this.updateForm.value.project_title);
      formData.append('project_description', this.updateForm.value.project_description);
      formData.append('project_start_date', this.updateForm.value.project_start_date);
      formData.append('project_end_date', this.updateForm.value.project_end_date);
      formData.append('project_status', status);
    formData.append('project_id', this.project_id);
    formData.append('is_active', this.updateForm.value.is_active);

    for (i = 0; i < this.images.length; i++) {
      var file: File = this.images[i];
      formData.append('image', file);
    }
    // formData.append('image', file);
    this.submitted = true;
    if (this.updateForm.valid) {
      this.apiService.edit_project(formData).subscribe((data: any) => {
        if (data.statuscode = 200) {
          swal({
            text: "Project Updated Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
          this.router.navigate(['/viewproject']);
        } else {
          swal({
            text: "Failed to Update project details.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    }
  }

  backClicked() {
    this.location.back();
  }

  onDelete(project_id, is_active) {
    var data = {
      project_id: project_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the project.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_project(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getproject();
                            swal({
                                text: 'Project Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the Project.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.deactivate_project(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getproject();
                            swal({
                                text: 'Project Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    }
  }

}
