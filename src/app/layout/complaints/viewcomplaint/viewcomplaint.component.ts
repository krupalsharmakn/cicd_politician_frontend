import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api-service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import swal from 'sweetalert';
import { MouseEvent } from '@agm/core';
import { AgmCoreModule } from '@agm/core';


@Component({
  selector: 'app-viewcomplaint',
  templateUrl: './viewcomplaint.component.html',
  styleUrls: ['./viewcomplaint.component.css']
})
export class ViewcomplaintComponent implements OnInit {
  complaintform: FormGroup;
  public complaintid;
  public complaints_list;
  public viewcomplaint;
  public actionArraydata;
  public images_array;
  public status_data;
  public lat;
  public lng;
  list;
   textarea:boolean=false;
  complaintstatuslist = [
    { value: 0, viewValue: 'Open' },
    { value: 3, viewValue: 'In Process' },
    { value: 2, viewValue: 'Completed' },
    { value: 1, viewValue: 'Irrelevant' },

  ];
  constructor(private location: Location,private apiService: ApiService,    private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.complaintform = this.formBuilder.group({
      status: ["", Validators.required],
      description: ["", Validators.required],
    })
    this.complaintid = localStorage.getItem('complaint_id');
    this.apiService.getcomplaint(this.complaintid).subscribe((actionArray:any) =>{
this.complaints_list=actionArray.data;
this.lat=JSON.parse(this.complaints_list.location[0]);
this.lng=JSON.parse(this.complaints_list.location[1]);
var viewcomplaint_status = this.complaintstatuslist.find(o => o.value === this.complaints_list.complaint_status);
this.complaintform.setValue({
  description: this.complaints_list.status_description || "",
  status:viewcomplaint_status.viewValue
})
this.images_array="http://";
    })
   

  }

  getcomplaint() {
   

  }

  updateStatus(eventdata){
    this.status_data=eventdata.target.value
    this.textarea=true;

  }

  onSubmit(){
    var dataupdate={
      complaint_id:this.complaintid,
      complaint_status:this.status_data,
      status_description:this.complaintform.value.description
    }
    this.apiService.update_complaint_status_description(dataupdate).subscribe((res: any) => {
      console.log("reeesss",res)
      if (res.statuscode = 200) {
        swal({
          text: "Complaint status updated successfully.",
          buttons: [false],
          dangerMode: true,
          timer: 3000
        });
        this.router.navigate(['/complaints']);
      } else {
        swal({
          text: "Failed to change complaint status",
          buttons: [false],
          dangerMode: true,
          timer: 3000
        });
      }

    })



  }
  backClicked() {
    this.location.back();
  }

// google maps zoom level
zoom: number = 8;
  
// initial center position for the map
// lat: number = 12.9716;
// lng: number = 77.5946;

clickedMarker(label: string, index: number) {
  console.log(`clicked the marker: ${label || index}`)
}

mapClicked($event: MouseEvent) {
  this.markers.push({
    lat: $event.coords.lat,
    lng: $event.coords.lng,
    draggable: true
  });
}

markerDragEnd(m: marker, $event: MouseEvent) {
  console.log('dragEnd', m, $event);
  console.log('dragEnd', this.complaints_list.location[0]);

  
}

markers: marker[] = [
  {
    lat:this.lat,
    lng: this.lng,
    label: 'A',
    draggable: true
  }
]
}


// just an interface for type safety.
interface marker {
lat: number;
lng: number;
label?: string;
draggable: boolean;
}
