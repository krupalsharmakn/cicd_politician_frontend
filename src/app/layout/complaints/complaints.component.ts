import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ApiService } from '../../shared/services/api-service';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import swal from 'sweetalert';
import { Location } from '@angular/common';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
export interface Status {
  value: Number;
  viewValue: string;
}

export interface Complaintlist {
  value: Number;
  viewValue: string;
}

export interface Element {
  sno: number;
  ward_no: string;
  quotation_number: number;
  complaint_description: string;
  complaint_category: number;
  place_name: string;
  complaint_status: number;
}

@Component({
  selector: 'app-complaints',
  templateUrl: './complaints.component.html',
  styleUrls: ['./complaints.component.css']
})


export class ComplaintsComponent implements OnInit {
  @ViewChild('tableloadedvalue') table: ElementRef;
  @ViewChild('tablefilterloadedvalue') tablefilter: ElementRef;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  value: string;
  viewValue: string;
  products: Element[];
  myControl: FormControl = new FormControl();
  public dataSource = new MatTableDataSource(this.products);
  public exceldata = new MatTableDataSource(this.products);
  public filterdataSource = new MatTableDataSource(this.products);
  public complaint_list;
  public viewvaluestatus = [];
  public datavalue = [];
  public statusvalue = [];
  public eventstatus;
  public list;
  public complaint_status_value;
  public complaint_status_viewvalue = [];
  public category_list;
  public status_value = {
    'status': []
  }
  filtertable: boolean = false;
  complaintstatuslist = [
    { value: 0, viewValue: 'Open' },
    { value: 3, viewValue: 'In Process' },
    { value: 2, viewValue: 'Completed' },
    { value: 1, viewValue: 'Irrelevant' },

  ];
  submitted = false;
  someModel;
  complaintstatus: Complaintlist[];
  toggleForm: boolean = false;
  datatable: boolean = true;
  excelColumns = ['sno', 'day_of_complaint', 'complaint_category', 'complaint_description', 'street_name', 'village', 'place_name', 'taluk', 'ward_no', 'complaint_status'];

  displayedColumns = ['sno', 'day_of_complaint', 'complaint_category', 'complaint_description', 'street_name', 'place_name', 'taluk', 'complaint_status'];
  filterform: FormGroup;
  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });
  constructor(private apiService: ApiService, private router: Router, private location: Location, private formBuilder: FormBuilder) { }
  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';
  ngOnInit() {

    this.list_complaints();
    this.listdata_to_excel();
    this.getcomplaint_category();
    this.someModel = 1;
    this.complaintstatus = [

      { value: 1, viewValue: 'Irrelevant' },
      { value: 0, viewValue: 'Open' },
      { value: 2, viewValue: 'In Process' },
      { value: 3, viewValue: 'Completed' }

    ]
    this.filterform = this.formBuilder.group({
      close_status: ['', Validators.required],
      complaint_category:['', Validators.required],
      taluk:['', Validators.required],
      town_panchayat:['', Validators.required],
      village_panchayat:['', Validators.required],
      ward_no:['', Validators.required],

    });

    this.dataSource.filterPredicate = (data, filter: string) => {
      console.log(data);

      if (this.option) {
        if (this.option) {
          return data.complaint_status == this.option;
        }
        return;
      }

    }


  }


  public exportExcel(jsonData: any[], fileName: string): void {

    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonData);
    const wb: XLSX.WorkBook = { Sheets: { 'data': ws }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    this.saveExcelFile(excelBuffer, fileName);
  }

  private saveExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: this.fileType });
    FileSaver.saveAs(data, fileName + this.fileExtension);
  }

  complaintfilter() {
    console.log("submit", this.filterform.value.close_status);
    this.filtertable = true;
    this.datatable = false;
    var data = {
      complaint_status: this.filterform.value.close_status,
      complaint_category:this.filterform.value.complaint_category,
      taluk: this.filterform.value.taluk,
      town_panchayat:this.filterform.value.town_panchayat,
      village_panchayat: this.filterform.value.village_panchayat,
      ward_no: this.filterform.value.ward_no
    }
    console.log("datatat", data)
    this.apiService.complaint_filter(data).subscribe((datavalue: any[]) => {
      console.log("datata", datavalue['data'])
      var filterdata = datavalue['data']
      this.dataSource = new MatTableDataSource();
      this.filterdataSource = new MatTableDataSource();
      this.dataSource.data = filterdata;
      this.filterdataSource = filterdata;
      console.log(this.dataSource);
      for (var i = 0; i < datavalue.length; i++) {
        var status = this.complaintstatuslist.find(o => o.value === datavalue[i].complaint_status);
        this.complaint_status_viewvalue.push(status.viewValue);
      }
      console.log("complaint_status_viewvalue", this.complaint_status_viewvalue)
      this.datavalue = this.dataSource.data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;



    });
  }
  get f() { return this.filterform.controls; }

  applyFilter(filterValue: string) {
    console.log(filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();

    // this.dataSource.filter = '' + Math.random() * 100;
    // console.log("this.dataSource.filter",this.dataSource.filter)
  }

  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'complaintlist.xlsx');
  }

  getcomplaint_category(){
    this.apiService.getcomplaint_category().subscribe((data: any[]) => {
      console.log("dattata",data['complaint_categories']);
      this.category_list=data['complaint_categories'][0]['complaint_categories'];
      console.log("this.category_list",this.category_list)
    })
  }

  exceldatafilterimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.tablefilter.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'sheet.xlsx');
  }
  list_complaints() {
    let allUsers = [];
    this.apiService.list_complaints().subscribe((data: any[]) => {
      console.log("datatatt", data)
      this.dataSource = new MatTableDataSource();
      this.dataSource.data = data;
      console.log(this.dataSource);
      for (var i = 0; i < data.length; i++) {
        var status = this.complaintstatuslist.find(o => o.value === data[i].complaint_status);
        this.complaint_status_viewvalue.push(status.viewValue);
      }
      console.log("complaint_status_viewvalue", this.complaint_status_viewvalue)
      this.status_value.status.push(this.complaint_status_viewvalue);
      console.log("this.status_value", this.status_value)

      console.log(this.dataSource);

      this.datavalue = this.dataSource.data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.list = ['open', 'delte'];


    });

  }

  listdata_to_excel() {
    this.apiService.list_complaints().subscribe((data: any[]) => {
      console.log("datatatt", data)
      this.exceldata = new MatTableDataSource();
      this.exceldata.data = data;
      console.log(this.exceldata);
      for (var i = 0; i < data.length; i++) {
        var status = this.complaintstatuslist.find(o => o.value === data[i].complaint_status);
        this.complaint_status_viewvalue.push(status.viewValue);
      }
      console.log("complaint_status_viewvalue", this.complaint_status_viewvalue)
      this.status_value.status.push(this.complaint_status_viewvalue);
      console.log("this.status_value", this.status_value)

      console.log(this.exceldata);


    });
  }
  onView(complaint_id) {
    localStorage.setItem('complaint_id', complaint_id);
    this.router.navigate(['/viewcomplaint']);
  }

  onwebsitechange(event) {
    this.eventstatus = event;

  }
  updateStatus(complaint_id, complaint_status) {
    if (complaint_status.target.value == 2) {
      swal({
        text: "Are you sure that you want to change complaint status to Completed state ?",
        buttons: ['Cancel', 'Ok'],
        dangerMode: true,
      })
        .then((value) => {
          if (value != null) {
            this.apiService.update_complaint_status(complaint_id, complaint_status.target.value).subscribe((data: any) => {
              if (data.statuscode = 200) {
                swal({
                  text: "Complaint status updated successfully.",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              } else {
                swal({
                  text: "Failed to change complaint status",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              }
            });
          } else {
            this.list_complaints()
          }
        });
    } else if (complaint_status.target.value == 3) {
      swal({
        text: "Change complaint status to In-Process state?",
        buttons: ['Cancel', 'Ok'],
        dangerMode: true,
      })
        .then((value) => {
          if (value != null) {
            this.apiService.update_complaint_status(complaint_id, complaint_status.target.value).subscribe((data: any) => {
              if (data.statuscode = 200) {
                swal({
                  text: "Complaint status updated successfully.",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              } else {
                swal({
                  text: "Failed to change complaint status",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              }
            });
          } else {
            this.list_complaints()
          }
        });
    } else if (complaint_status.target.value == 0) {
      swal({
        text: "Are you sure that you want to make complaint status to Open state?",
        buttons: ['Cancel', 'Ok'],
        dangerMode: true,
      })
        .then((value) => {
          if (value != null) {
            this.apiService.update_complaint_status(complaint_id, complaint_status.target.value).subscribe((data: any) => {
              if (data.statuscode = 200) {
                swal({
                  text: "Complaint status updated successfully.",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              } else {
                swal({
                  text: "Failed to change complaint status",
                  buttons: [false],
                  dangerMode: true,
                  timer: 3000
                });
              }
            });
          } else {
            this.list_complaints()
          }
        });
    } else {
      if (complaint_status.target.value == 1) {
        swal({
          text: "Are you sure you want to reject the complaint ?",
          buttons: ['Cancel', 'Ok'],
          dangerMode: true,
        })
          .then((value) => {
            if (value != null) {
              this.apiService.update_complaint_status(complaint_id, complaint_status.target.value).subscribe((data: any) => {
                if (data.statuscode = 200) {
                  swal({
                    text: "complaint rejected successfully",
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                } else {
                  swal({
                    text: "Failed to reject complaint",
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                }
              });
            } else {
              this.list_complaints()
            }
          });
      }
    }
  }

  filterformenable = false;
  // table = true;


  onCloseHandled() {
    this.filterformenable = false;
    this.filtertable = false;
    this.filterform.reset()

    this.datatable = true;
    // this.filterform.setValue({
    //   close_status: ''
    // })

    this.filterform.setValue({
      close_status: '',
      complaint_category:'',
      taluk:'',
      town_panchayat:'',
      village_panchayat:'',
      ward_no:''


    })    
   
    this.list_complaints()
    // this.table = true;
  }
  onopenHandled(tag) {
    this.filterformenable = true;
    // this.table = false;
  }
  // applyFilter() {

  //   this.dataSource.filter = '' + Math.random() * 100;
  //   console.log("this.dataSource.filter",this.dataSource.filter)
  // }

  backClicked() {
    this.location.back();
  }
  option;
  data;
  onFilter() {
    this.submitted = true;
    if (this.filterform.valid) {
      console.log(this.filterform.value.close_status);
      this.option = this.filterform.value.close_status
      this.apiService.list_complaint_status(this.option).subscribe((data: any[]) => {
        console.log(data);
        this.data = data;
        this.dataSource.data = this.data.data;
        this.datavalue = this.dataSource.data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.list = ['open', 'delte'];
      });
    }
    else {
      swal({
        text: "Please fill all the details",
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }

  }
}









