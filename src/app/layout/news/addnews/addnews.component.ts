import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
import { Location } from '@angular/common';

@Component({
  selector: 'app-addnews',
  templateUrl: './addnews.component.html',
  styleUrls: ['./addnews.component.css']
})
export class AddnewsComponent implements OnInit {
  addnewsform: FormGroup;
  public profileimage;
  public uploadimage;
  loader = false;
  submitted = false;
  myFiles: string[] = [];
  title = 'fileupload';
  remark = '';
  sMsg: string = '';
  StudentIdUpdate: string;
  constructor(private location: Location, private apiService: ApiService, private router: Router, private formBuilder: FormBuilder) {

  }
  ngOnInit() {
    const file: File = this.profileimage;
    this.addnewsform = this.formBuilder.group({
      news_title: ['', Validators.required],
      news_description: ['', Validators.required],
      news_category: ['', Validators.required],
      news_reported_on: ['', Validators.required]
      // image: ['']
    });
  }
  get f() { return this.addnewsform.controls; }
  imagepath
  images = new Array();
  onSubmit() {
    this.imagepath = ''
    this.images = []
    console.log(this.myFiles);

    const frmData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      this.imagepath = this.myFiles[i];
      console.log(this.imagepath);
      this.images.push(this.imagepath)
      if (i == 0) {
        frmData.append("remark", this.remark);
      }
    }
    console.log("image upload", JSON.parse(JSON.stringify(this.images)));
    const updatenews: FormData = new FormData();
    updatenews.append('news_title', this.addnewsform.value.news_title);
    updatenews.append('news_description', this.addnewsform.value.news_description);
    updatenews.append('news_category', this.addnewsform.value.news_category);
    updatenews.append('news_reported_on', this.addnewsform.value.news_reported_on);
    for (i = 0; i < this.images.length; i++) {
      var file: File = this.images[i];
      updatenews.append('image', file);
    }

    this.submitted = true;
    if (this.addnewsform.valid) {
      this.loader = false;
      this.apiService.add_news(updatenews).subscribe((data: any) => {
        if (data.statuscode == 200) {
          swal({
            text: 'News added Successfully.',
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
          this.router.navigate(['/news']);
          this.addnewsform.reset();
        } else {
          swal({
            text: 'Something went wrong! Please try again.',
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    } else {
      swal({
        text: 'Please fill all the details',
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }

  // fileChange(e) {
  //   const profiles = e.target.files[0];
  //   this.profileimage = profiles;
  //   const reader = new FileReader();
  //   reader.onload = () => {
  //     this.uploadimage = reader.result;
  //   };
  //   reader.readAsDataURL(profiles);
  // }

  backClicked() {
    this.location.back();
  }
  urls = [];
  getFileDetails(e) {
    //console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);

      var reader = new FileReader();

      reader.onload = (event: any) => {
        console.log(event.target.result);
        this.urls.push(event.target.result);
      }

      reader.readAsDataURL(e.target.files[i]);
    }
  }

  // uploadFiles() {  
  //   const frmData = new FormData();  
  //   for (var i = 0; i < this.myFiles.length; i++) {  
  //     frmData.append("fileUpload", this.myFiles[i]);  
  //     if (i == 0) {  
  //       frmData.append("remark", this.remark);  
  //     }  
  //   }
  //   console.log("image upload",frmData);
  // }

  // name = 'Angular 4';
  // urls = [];
  // onSelectFile(event) {
  //   if (event.target.files && event.target.files[0]) {
  //       var filesAmount = event.target.files.length;
  //       for (let i = 0; i < filesAmount; i++) {
  //               var reader = new FileReader();

  //               reader.onload = (event:any) => {
  //                  console.log(event.target.result);
  //                  this.urls.push(event.target.result); 
  //               }

  //                reader.readAsDataURL(event.target.files[i]);
  //       }
  //   }
  //   console.log(this.urls);

  // }
}