import {Component, OnInit, ViewChild,ElementRef} from '@angular/core';
import {ApiService} from '../../shared/services/api-service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as XLSX from 'xlsx';

import {
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/animations';
import {Router} from '@angular/router';
import swal from 'sweetalert';


@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  animations: [
    trigger('slideInOut', [
      state(
          'in',
          style({
            transform: 'translate3d(0, 0, 0)'
          })
      ),
      state(
          'out',
          style({
            transform: 'translate3d(100%, 0, 0)'
          })
      ),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ])
  ]

})
export class NewsComponent implements OnInit {
  @ViewChild('tableloadedvalue') table: ElementRef;
  @ViewChild('tablefilterloadedvalue') tablefilter: ElementRef;
  public items: Array<any>;
  public newsdata;
  toggleForm = false;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['sno', 'news_title', 'news_description', 'action'];
  displayedExcelColumns:string[]=['sno', 'news_title','news_category','news_description','news_reported_on']
  places: Array<any> = [];
  slideState = 'out';
  public toggleenable: Boolean = false;
  dataSource: any;
  excelData:any;
  public newslist;
  loader = false;
  filterformenable = false;
  filterform: FormGroup;
  public category_list;
  public news_category_list;
  public news_catgory_data;

  images_array="http://";

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }

  constructor(private apiService: ApiService, private router: Router,private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.list_news();
    this.get_all_category();
    this.filterform=this.formBuilder.group({
      news_category:['', Validators.required],
    })
  }

  list_news() {
    this.apiService.list_news().subscribe(res => {
          this.newslist = res;
          this.loader = false;
          this.dataSource = new MatTableDataSource();
          this.excelData = new MatTableDataSource();
          this.excelData.data=this.newslist;
          this.dataSource.data = this.newslist;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },
        err => console.error(err)
    );
  }

  onopenHandled() {
    this.filterformenable = true;
  }

  onCloseHandled() {
    this.filterformenable = false;
    this.filterform.reset()

    // this.filterform.setValue({
    //   close_status: ''
    // })

    this.filterform.setValue({
      news_category: '',
    })     
    this.list_news()
    // this.table = true;
  }
  news_filter(){
    console.log("news filter",this.filterform.value.news_category);
    var news_category=this.filterform.value.news_category
    this.apiService.list_news_by_category(news_category).subscribe(res => {
     this.news_catgory_data=res['data']
        // this.category_list=res;
       console.log("res",this.news_catgory_data);
       this.dataSource = new MatTableDataSource();
       this.dataSource = new MatTableDataSource();
       this.excelData = new MatTableDataSource();
       this.excelData.data=this.news_catgory_data;
       this.dataSource.data = this.news_catgory_data;
       this.dataSource.sort = this.sort;
       this.dataSource.paginator = this.paginator;
  
      //  console.log("res",this.category_list.complaint_categories.news_category)
      //  this.news_category_list=this.category_list.complaint_categories.news_category
      },
      err => console.error(err)
  );
  }


  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'newslist.xlsx');
  }

  get_all_category(){
    this.apiService.get_all_news_category().subscribe(res => {
    var catgory_list=res
      this.category_list=res;
     console.log("res",this.category_list)

     console.log("res",this.category_list.complaint_categories.news_category)
     this.news_category_list=this.category_list.complaint_categories.news_category
    },
    err => console.error(err)
);
  }
  toggleMenu(news_id) {
    this.toggleenable = true;
    this.slideState = this.slideState === 'out' ? 'in' : 'out';
    this.apiService.get_news(news_id).subscribe(
        res => {
          console.log("vieww news",res)
          this.newsdata = res;
          this.newsdata = this.newsdata.data;
        },
        err => console.error(err)
    );
  }

  onView(news_id) {
    localStorage.setItem('news_id', news_id);
    this.router.navigate(['/viewnews']);
  }

  toggleclose() {
    this.slideState = this.slideState === 'in' ? 'out' : 'in';
    this.newsdata = '';
    this.toggleenable = false;
  }

  onEdit(news_id) {
    localStorage.setItem('news_id', news_id);
    this.router.navigate(['/editnews']);
  }

  onDelete(news_id, is_active) {
    var data = {
      news_id: news_id,
      is_active: is_active
    };
    if (is_active == true) {
      swal({
        text: 'Are you sure?. Confirm to Activate the News.',
        buttons: ['Cancel', 'Ok'],
        dangerMode: true
      })
          .then((value) => {
            if (value) {
              this.apiService.delete_news(data).subscribe((data: any) => {
                if (data.statuscode = 204) {
                  this.list_news();
                  swal({
                    text: 'News Activated Successfully.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                } else {
                  swal({
                    text: 'Something went wrong! Please try again.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                }
              });
            }
          });
    } else {
      swal({
        text: 'Are you sure?. Confirm to Deactivate the News.',
        buttons: ['Cancel', 'Ok'],
        dangerMode: true
      })
          .then((value) => {
            if (value) {
              this.apiService.delete_news(data).subscribe((data: any) => {
                if (data.statuscode = 204) {
                  this.list_news();
                  swal({
                    text: 'News Deactivated Successfully.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                } else {
                  swal({
                    text: 'Something went wrong! Please try again.',
                    buttons: [false],
                    dangerMode: true,
                    timer: 3000
                  });
                }
              });
            }
          });
    }
  }

}
