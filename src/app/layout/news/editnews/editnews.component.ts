import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../../shared/services/api-service';
import swal from 'sweetalert';
import { log } from 'util';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

export interface Categorylist {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-editnews',
  templateUrl: './editnews.component.html',
  styleUrls: ['./editnews.component.css']
})


export class EditnewsComponent implements OnInit {

  editnewsform: FormGroup
  public newsdata;
  news_status: Categorylist[];

  public currentuser;
  public image;
  public uploadimage;
  public profileimage;
  loader = false;
  myFiles: string[] = [];
  public news_details;
  private submitted: boolean;
  public afterchange=false;

  images_array="http://";
  constructor(private location: Location,private router: Router, public apiService: ApiService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.getnews();
    this.news_status = [

      { value: 'department', viewValue: 'Department' },
      { value: 'assembly', viewValue: 'Assembly' },
      { value: 'media', viewValue: 'Media' },
      { value: 'party', viewValue: 'party' }

    ]
    this.editnewsform = this.formBuilder.group({
      news_id: ['', Validators.required],
      news_title: ['', Validators.required],
      news_description: ['', Validators.required],
      news_category: ['', Validators.required],
      news_reported_on:['', Validators.required],
    });
  }
  category(data){
    console.log("datataaaa",data)
  }

  getnews() {
    const news_id = localStorage.getItem("news_id");
    this.loader = true;
    this.apiService.get_news(news_id).subscribe(
        res => {
          this.newsdata = res;
          this.loader = false;
          if (this.newsdata.statuscode == 200) {
            const newsdatas = this.newsdata.data;
            this.news_details=this.newsdata.data
            this.uploadimage = newsdatas.news_images[0] || '';

this.myFiles = newsdatas.news_images;
            for(let i=0;i<newsdatas.news_images.length;i++)
            this.urls.push(newsdatas.news_images[i]); 
            console.log("newsdatas",this.urls)

            this.editnewsform.setValue({
              news_id: newsdatas.news_id,
              news_title: newsdatas.news_title || '',
              news_description: newsdatas.news_description || '',
              news_category:newsdatas.news_category || '',
              news_reported_on:newsdatas.news_reported_on || '',

            });
          }
          console.log("this.editnewsform",this.editnewsform.value)

        },
        err => console.error(err)
    );
  }

  fileChange(e) {
    const profiles = e.target.files[0];
    this.profileimage = profiles;
    const reader = new FileReader();
    reader.onload = () => {
      this.uploadimage = reader.result;
    };
    reader.readAsDataURL(profiles);
  }
  urldata = []
  urls = [];
  getFileDetails(e) 
  {  
    this.afterchange=true;

    for (var i = 0; i < e.target.files.length; i++) 
    {  
      this.myFiles.push(e.target.files[i]); 
      var reader = new FileReader();
                reader.onload = (event:any) => {
                  this.urldata.push(event.target.result);

                  //  this.urls.push(event.target.result); 
                }
                 reader.readAsDataURL(e.target.files[i]);
    }  
  } 
index=-1;
deletedimages = [];
  removeimage(url)
  {
    console.log("working",this.urls.indexOf(url));
    console.log("working123",this.urls,typeof(this.urls));
    this.urls.splice(this.urls.indexOf(url), 1); 
    this.myFiles.splice(this.myFiles.indexOf(url), 1);
    this.deletedimages.push(url);
  }
  // index = -1;
// console.log(index);
  imagepath
  images = new Array();

  onSubmit() {
    console.log("this.editnewsform.value",this.editnewsform.value)
    var department;
    this.images = []

    const frmData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      this.imagepath = this.myFiles[i];
      console.log(this.imagepath);
      this.images.push(this.imagepath)
     
    }
    const updatenews: FormData = new FormData();
    updatenews.append('news_id', this.editnewsform.value.news_id);
    updatenews.append('news_title', this.editnewsform.value.news_title);
    updatenews.append('news_category', this.editnewsform.value.news_category);
    updatenews.append('news_reported_on', this.editnewsform.value.news_category);

    updatenews.append('news_description', this.editnewsform.value.news_description);
    this.myFiles = this.urls;
    console.log("images = ",this.myFiles,this.deletedimages)
    for (i = 0; i < this.images.length; i++) {
      var file: File = this.images[i];
      updatenews.append('image', file);
    }
    this.submitted = true;
    if (this.editnewsform.valid) {
      this.apiService.edit_news(updatenews).subscribe((res: any) => {
        if (res.statuscode == 200) {
          swal({
            text: 'News Updated Successfully.',
            buttons: [false],
            dangerMode: true,
            timer: 3000

          });
          this.router.navigate(['/news']);
        } else {
          swal({
            text: 'Something went wrong! Please try again.',
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
        }
      });
    } else {
      swal({
        text: 'Please fill all the details',
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }
  }

  backClicked() {
    this.location.back();
  }

}
