import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../shared/services/api-service';
import {Router} from '@angular/router';
import { Location } from '@angular/common';
import swal from 'sweetalert';

@Component({
  selector: 'app-viewnews',
  templateUrl: './viewnews.component.html',
  styleUrls: ['./viewnews.component.css']
})
export class ViewnewsComponent implements OnInit {
public newsdata;
public newsid;
urls = [];
  constructor(private apiService: ApiService,private router: Router,private location: Location) { }

  ngOnInit() {
    this.newsid = localStorage.getItem('news_id');
    this.get_news_data()

  }

  get_news_data(){

  
  this.apiService.get_news(this.newsid).subscribe(
    res => {
      this.newsdata = res;
      this.newsdata = this.newsdata.data;
      for(let i=0;i<this.newsdata.news_images.length;i++)
            this.urls.push(this.newsdata.news_images[i]); 
      console.log("vieww news1111",this.newsdata,"dfsdfsdf",this.urls)

    },
    err => console.error(err)
);
  }


  backClicked() {
    this.location.back();
  }

  
  onDelete(news_id, is_active) {
    console.log("newssssss",news_id,"asdasdasdasd",is_active)
    var data = {
      news_id: news_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the project.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_news(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.get_news_data();
                            swal({
                                text: 'News Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the News.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_news(data).subscribe((data: any) => {
                      console.log("datatatt",data.statuscode)
                        if (data.statuscode = 204) {
                          this.get_news_data();
                            swal({
                                text: 'News Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 3000
                            });
                        }
                    });
                }
            });
    }
  }
  
}