import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
import { Location } from '@angular/common';
@Component({
  selector: 'app-viewpartyworker',
  templateUrl: './viewpartyworker.component.html',
  styleUrls: ['./viewpartyworker.component.css']
})
export class ViewpartyworkerComponent implements OnInit {
partyworker;
worker_id;
constructor(private apiService: ApiService, private router: Router, private formBuilder: FormBuilder,private location: Location) { }

  ngOnInit() {
    this.partyworker="name";
    this.worker_id = localStorage.getItem('worker_id');
    this.getpartyworker()
  }

  getpartyworker(){
    this.apiService.get_partyworker(this.worker_id).subscribe(
      res => {
        this.partyworker = res;
       this.partyworker = this.partyworker.data;
      },
      err => console.error(err)
  );
  }

  onEdit(){
    localStorage.setItem('worker_editid', this.worker_id);
     this.router.navigate(['/addpartyworker']);

  }
  onDelete(work_id, is_active) {
    var data = {
      party_worker_id: work_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the Party worker.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_worker(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getpartyworker()
                          swal({
                                text: 'Party worker Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the Party worker.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_worker(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                          this.getpartyworker()
                          swal({
                                text: 'Party worker Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    }
  }

  backClicked() {
    this.location.back();
  }
}
