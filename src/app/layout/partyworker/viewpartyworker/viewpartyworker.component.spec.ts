import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpartyworkerComponent } from './viewpartyworker.component';

describe('ViewpartyworkerComponent', () => {
  let component: ViewpartyworkerComponent;
  let fixture: ComponentFixture<ViewpartyworkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpartyworkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpartyworkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
