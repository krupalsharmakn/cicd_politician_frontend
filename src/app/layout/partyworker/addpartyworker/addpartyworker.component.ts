import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/shared/services/api-service';
import swal from 'sweetalert';
import { Location } from '@angular/common';
@Component({
  selector: 'app-addpartyworker',
  templateUrl: './addpartyworker.component.html',
  styleUrls: ['./addpartyworker.component.css']
})
export class AddpartyworkerComponent implements OnInit {
  addpartyform: FormGroup;
  submitted = false;
  today=new Date();
workeredit_id;
partyworker;
todaydate:Date;
age;
category_list;
  taluk_list;
  ward_list;
  editdata=false;
  district_list;
  talukname;
  wardname;
  constructor(private apiService: ApiService, private router: Router, private formBuilder: FormBuilder,private location: Location) { }

  ngOnInit() {
    this.getcategory_list();
    this.workeredit_id=localStorage.getItem('worker_editid');
    console.log("this.editdept",this.workeredit_id)
    if(this.workeredit_id){
      this.getpartyworker();
      this.editdata=true;
    }
    this.addpartyform = this.formBuilder.group({
      party_worker_id:[''],
      name: ['', Validators.required],
      email_id: ['', [Validators.required,Validators.email]],
      phone_number: ['', [Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      alternative_number: ['',Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")],
      dob: ['', Validators.required],
      gender: [''],
      worker_type: ['',Validators.required],
      ward:['',Validators.required],
      taluk:['',Validators.required],
      district:['',Validators.required],
      pincode:['',Validators.required],
      // state:['',Validators.required],
    });
  }

  get f() { return this.addpartyform.controls; }

  getcategory_list(){
    console.log("334343fffffffg")
    this.apiService.getcomplaint_category().subscribe(
      res => {
        this.category_list = res;
        this.district_list = this.category_list.complaint_categories.district;
      },
      err => console.error(err)
  );
  }

  getpartyworker(){
    this.apiService.get_partyworker(this.workeredit_id).subscribe(
      res => {
        this.partyworker = res;
       const partydatas = this.partyworker.data;
       var datevalue=this.partyworker.data.DOB;
       var d1 = new Date(this.partyworker.data.DOB),
       month = '' + (d1.getMonth() + 1),
       day = '' + d1.getDate(),
       year = d1.getFullYear();
       var start_date = day+'-'+month+'-'+year;
       console.log("start_date",start_date)
       console.log("d1",d1)
       console.log("ssff",datevalue)
       this.addpartyform.setValue({
        party_worker_id:partydatas.party_worker_id,
        name:partydatas.name,
        email_id:partydatas.email,
        phone_number:partydatas.phone_number,
        alternative_number:partydatas.alternative_number,
        dob:this.partyworker.data.DOB,
        gender:partydatas.gender,
        worker_type:partydatas.party_worker_type,
        ward:partydatas.panchayath_ward,
        taluk:partydatas.taluk,
        district:partydatas.district,
        pincode:partydatas.pin,
        // state:partydatas.state


       })

       this.talukname= this.addpartyform.value.taluk;
       this.wardname=this.addpartyform.value.ward;
       console.log(" this.talukname", this.talukname)
      },
      err => console.error(err)
  );
  }
  onSubmit(){
    console.log("this.addpartyform",this.addpartyform.value);
    this.submitted = true;
    var agerange;
    this.todaydate =this.addpartyform.value.dob;
    var timeDiff = Math.abs(Date.now() - new Date(this.todaydate).getTime());
     this.age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
     if(this.age<25){
      agerange='18-25';
     }else if(this.age>=25 && this.age<=30){
      agerange='25-30';

     }else if(this.age>=30 && this.age<=40){
      agerange='30-40';

     }
     console.log("agerange",agerange)
    if(this.addpartyform.valid){
      this.submitted = false;

      if(this.addpartyform.value.party_worker_id){

        const editwoker: FormData = new FormData();
        editwoker.append('party_worker_id', this.addpartyform.value.party_worker_id);
        editwoker.append('name', this.addpartyform.value.name);
        editwoker.append('email', this.addpartyform.value.email_id);
        editwoker.append('phone_number', this.addpartyform.value.phone_number);
        editwoker.append('alternative_number', this.addpartyform.value.alternative_number);
        editwoker.append('DOB', this.addpartyform.value.dob);
        editwoker.append('gender', this.addpartyform.value.gender);
        editwoker.append('party_worker_type', this.addpartyform.value.worker_type);
        editwoker.append('panchayath_ward', this.addpartyform.value.ward);
        editwoker.append('taluk', this.addpartyform.value.taluk);
        editwoker.append('district', this.addpartyform.value.district);
        editwoker.append('age', this.age);
        editwoker.append('pin', this.addpartyform.value.pincode);
        // editwoker.append('state', this.addpartyform.value.state);
        editwoker.append('age_range', agerange);


   
        this.apiService.edit_partyworker(editwoker).subscribe((data: any) => {
          console.log("data",data)
          if (data.statuscode == 200) {
            swal({
              text: 'Party worker updated Successfully.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
            this.router.navigate(['/partyworker']);
            this.addpartyform.reset();
          } else {
            swal({
              text: 'Something went wrong! Please try again.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          }
        });
      }else{
        console.log("this.addpartyform.value",this.addpartyform.value)
        const addworker: FormData = new FormData();
        addworker.append('name', this.addpartyform.value.name);
        addworker.append('email', this.addpartyform.value.email_id);
        addworker.append('phone_number', this.addpartyform.value.phone_number);
        addworker.append('alternative_number', this.addpartyform.value.alternative_number);
        addworker.append('DOB', this.addpartyform.value.dob);
        addworker.append('gender', this.addpartyform.value.gender);
        addworker.append('party_worker_type', this.addpartyform.value.worker_type);
        addworker.append('panchayath_ward', this.addpartyform.value.ward);
        addworker.append('taluk', this.addpartyform.value.taluk);
        addworker.append('district', this.addpartyform.value.district);
        addworker.append('age', this.age);
        addworker.append('pin', this.addpartyform.value.pincode);
        // addworker.append('state', this.addpartyform.value.state);
        addworker.append('age_range', agerange);


        this.apiService.add_partyworker(addworker).subscribe((data: any) => {
          console.log("data",data)
          if (data.statuscode == 200) {
            swal({
              text: 'Party worker added Successfully.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
            this.router.navigate(['/partyworker']);
            this.addpartyform.reset();
          }
          if (data.statuscode == 304) {
            swal({
              text: 'Party worker already exists with that mobile number.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          }
           else {
            swal({
              text: 'Something went wrong! Please try again.',
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          }
        });

      }

    }else {
      swal({
        text: 'Please fill all the details',
        buttons: [false],
        dangerMode: true,
        timer: 3000
      });
    }

  }





  loadTaluk(eventdata: any) {
    this.editdata=false;
    this.addpartyform.controls.pincode.setValue('')
    console.log("eventdata", eventdata.target.value);
    var datalist = this.district_list.find(o => o.name == eventdata.target.value) || '';
    this.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)

    // var datalist=this.taluk_list.findIndex(item=>item.taluk_name===eventdata.target.value);
    this.ward_list = datalist.town_panchayat
    console.log("datalist", this.ward_list)
  }


  loadPanchayat(eventdata: any){
    var datalist = this.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.ward_list = datalist.panchayath_ward;
  }

  upDatepin(eventdata){
    var datalist = this.ward_list.find(o => o.name == eventdata.target.value) || '';
    console.log("datalist",datalist.pin)
    this.addpartyform.controls.pincode.setValue(datalist.pin)
    // this.pinvalue=datalist.pin;
    
  }
  backClicked() {
    this.location.back();
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
