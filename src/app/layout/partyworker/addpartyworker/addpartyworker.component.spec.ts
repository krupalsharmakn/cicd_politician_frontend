import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddpartyworkerComponent } from './addpartyworker.component';

describe('AddpartyworkerComponent', () => {
  let component: AddpartyworkerComponent;
  let fixture: ComponentFixture<AddpartyworkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddpartyworkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddpartyworkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
