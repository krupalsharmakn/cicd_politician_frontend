import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartyworkerComponent } from './partyworker.component';

describe('PartyworkerComponent', () => {
  let component: PartyworkerComponent;
  let fixture: ComponentFixture<PartyworkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartyworkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartyworkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
