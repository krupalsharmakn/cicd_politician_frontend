import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import {ApiService} from '../../shared/services/api-service';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as XLSX from 'xlsx';
import {Router} from '@angular/router';
import swal from 'sweetalert';

@Component({
  selector: 'app-partyworker',
  templateUrl: './partyworker.component.html',
  styleUrls: ['./partyworker.component.css']
})
export class PartyworkerComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('tableloadedvalue') table: ElementRef;
  @ViewChild('tablefilterloadedvalue') tablefilter: ElementRef;
  dataSource: any;
  excelData:any;
  partylist;
  filterformenable = false;
  displayedColumns: string[] = ['sno', 'name','phone_number','party_worker_type','taluk','ward','district', 'action'];
  displayedExcelColumns: string[] = ['sno', 'name','phone_number','alternative_number','party_worker_type','email','gender','dob','ward','taluk','district'];
  filterform: FormGroup;
filterdata;
category_list;
  taluk_list;
  ward_list;
ageselect=false;
district_list;
  constructor(private apiService: ApiService, private formBuilder: FormBuilder,private router: Router,) { }

  ngOnInit() {
    localStorage.removeItem('worker_editid');
    this.list_partyworker();
    this.getcategory_list();
    this.filterform=this.formBuilder.group({
      worker_type:['', Validators.required],
      panchayath_ward:['', Validators.required],
      taluk:['', Validators.required],
      district:['', Validators.required],
      age:['', Validators.required],
      pincode:[''],
      gender:['']

    }) 
  }

  get f() { return this.filterform.controls; }
  getcategory_list(){
    console.log("334343fffffffg")
    this.apiService.getcomplaint_category().subscribe(
      res => {
        this.category_list = res;
        this.district_list = this.category_list.complaint_categories.district;
      },
      err => console.error(err)
  );
  }



  loadTaluk(eventdata: any) {
    console.log("eventdata", eventdata.target.value);
    var datalist = this.district_list.find(o => o.name == eventdata.target.value) || '';
    this.taluk_list = datalist.taluk;
    console.log("datalistssd", datalist)

    // var datalist=this.taluk_list.findIndex(item=>item.taluk_name===eventdata.target.value);
    this.ward_list = datalist.town_panchayat
    console.log("datalist", this.ward_list)
  }


  loadPanchayat(eventdata: any){
    var datalist = this.taluk_list.find(o => o.name == eventdata.target.value) || '';
    this.ward_list = datalist.panchayath_ward;
  }

  upDatepin(eventdata){
    var datalist = this.ward_list.find(o => o.name == eventdata.target.value) || '';
    console.log("datalist",datalist.pin);
    this.filterform.controls.pincode.setValue(datalist.pin)

    // this.pinvalue=datalist.pin;
    
  }


  list_partyworker() {
    this.apiService.list_partyworker().subscribe(res => {
          this.partylist = res;
          this.dataSource = new MatTableDataSource();
          this.excelData = new MatTableDataSource();
          this.excelData.data=this.partylist;
          this.dataSource.data = this.partylist;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log("lissttt", this.partylist)

        },
        err => console.error(err)
    );
  }

  exceldataimport() {
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    console.log("this.table.nativeElement", this.table.nativeElement)
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'partyworkerslist.xlsx');
  }


  onView(worker_id) {
    console.log("iddd",worker_id)
    localStorage.setItem('worker_id', worker_id);
  this.router.navigate(['/viewpartyworker']);
  }

  onopenHandled() {
    this.filterformenable = true;
  }

  onCloseHandled() {
    this.filterformenable = false;
    this.filterform.reset()
    this.filterform.setValue({
      worker_type: '',
      panchayath_ward:'',
      taluk:'',
      district:'',
      pincode:'',
      age:'',
      gender:''

    })     
    this.list_partyworker()
  }
  partyworkerfilter(){
    var fileterlist={
      party_worker_type: this.filterform.value.worker_type,
      panchayath_ward:this.filterform.value.panchayath_ward,
      taluk:this.filterform.value.taluk,
      district:this.filterform.value.district,
      age_range: this.filterform.value.age,
      pin: this.filterform.value.pincode,
      gender: this.filterform.value.gender,


    }
    this.apiService.filter_by_type(fileterlist).subscribe(res => {
      this.filterdata=res['data'];
        console.log("res",this.filterdata);
        this.dataSource = new MatTableDataSource();
        this.dataSource = new MatTableDataSource();
        this.excelData = new MatTableDataSource();
        this.excelData.data=this.filterdata;
        this.dataSource.data = this.filterdata;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
 
       },
       err => console.error(err)
   );
   }
   onEdit(worker_id) {
    localStorage.setItem('worker_editid', worker_id);
    this.router.navigate(['/addpartyworker']);
}

selectworktype(eventdata)
{
console.log("eventdata",)
if(eventdata.target.value=='Youth Member'){
  this.ageselect=true;
}
}   onDelete(worker_id, is_active) {
    var data = {
      party_worker_id: worker_id,
        is_active: is_active
    };
    if (is_active == true) {
        swal({
            text: 'Are you sure?. Confirm to Activate the Party Worker.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_worker(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                            this.list_partyworker();
                            swal({
                                text: 'Party Worker Activated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    } else {
        swal({
            text: 'Are you sure?. Confirm to Deactivate the Party Worker.',
            buttons: ['Cancel', 'Ok'],
            dangerMode: true
        })
            .then((value) => {
                if (value) {
                    this.apiService.delete_worker(data).subscribe((data: any) => {
                        if (data.statuscode = 204) {
                            this.list_partyworker();
                            swal({
                                text: 'Party Worker Deactivated Successfully.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        } else {
                            swal({
                                text: 'Something went wrong! Please try again.',
                                buttons: [false],
                                dangerMode: true,
                                timer: 2000
                            });
                        }
                    });
                }
            });
    }
  }


  sortData(sort: MatSort) {
    const data = this.partylist.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource = data;
      return;
    }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'phone_number': return compare(a.phone_number, b.phone_number, isAsc);
        case 'party_worker_type': return compare(a.party_worker_type, b.party_worker_type, isAsc);
        case 'taluk': return compare(a.taluk, b.taluk, isAsc);
        case 'ward': return compare(a.ward, b.ward, isAsc);
        case 'district': return compare(a.district, b.district, isAsc);

        default: return 0;
      }
    });
  }


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  
}


function compare(a: number | string, b: number | string, isAsc: boolean) { 
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}