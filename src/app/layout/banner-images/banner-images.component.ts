import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../shared/services/api-service';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
import swal from 'sweetalert';

@Component({
  selector: 'app-banner-images',
  templateUrl: './banner-images.component.html',
  styleUrls: ['./banner-images.component.css']
})
export class BannerImagesComponent implements OnInit {
public images_data;
public images:true;
myFiles: string[] = [];
addimage_file=false;
image_submit=false;
click_image=true;
remark = '';


  constructor(private apiService: ApiService,private router: Router,private location: Location) { }


  ngOnInit() {
    this.getbanner()
    this.images=true;
  }

  getbanner(){
    var image=""
    this.apiService.add_banner_images(image).subscribe(
      res => {
        // console.log("res",res)
        this.images_data = res;
        this.images_data = this.images_data.banner_images;
        console.log("this.images_data",this.images_data)
        // for(let i=0;i<this.images_data.news_images.length;i++)
        //       this.urls.push(this.images_data.banner_images[i]); 
        // console.log("vieww news1111",this.newsdata,"dfsdfsdf",this.urls)
  
      },
      err => console.error(err)
  );
  }
  addimage(){
    this.addimage_file=true;
    this.image_submit=true;
    this.click_image=false;;

  }

  backClicked(){
    this.addimage_file=false;
    // location.reload();
    this.urls=[]
    this.image_submit=false;
    this.click_image=true;;




  }
  imagepath;
  imagesarray = new Array();

  uploadimage(){
    console.log("this.urls",this.urls)
    this.imagepath = ''
    const updateimage: FormData = new FormData();

    const frmData = new FormData();
    for (var i = 0; i < this.myFiles.length; i++) {
      this.imagepath = this.myFiles[i];
      console.log(this.imagepath);
      this.imagesarray.push(this.imagepath)
      if (i == 0) {
        frmData.append("remark", this.remark);
      }
    }

    for (i = 0; i < this.imagesarray.length; i++) {
      var file: File = this.imagesarray[i];
      updateimage.append('image', file);
    }

    var image=""
    this.apiService.add_banner_images(updateimage).subscribe(
      (res:any )=> {
         console.log("res",res)

         if (res.statuscode == 200) {
          swal({
            text: "Banner Image updated Successfully.",
            buttons: [false],
            dangerMode: true,
            timer: 3000
          });
          // this.ngOnInit();
          // this.addimage_file=false;
          // this.image_submit=false;
          // this.click_image=true;
          location.reload();
          this.router.navigate(['/banner_images']);
        } else {
          swal({
            text: "Something went wrong! Please try again.",
            buttons: [false],
            dangerMode: true,
            timer: 2000
          });
        }
        this.images_data = res;
        this.images_data = this.images_data.banner_images;
        console.log("this.images_data",this.images_data)
      },
      err => console.error(err)
  );
  }
  delete_image(image_url)
  {
console.log("image_url",image_url);
const updateimagedata: FormData = new FormData();
updateimagedata.append('deleteimage',image_url);
var deletedata={
  deleteimage:image_url
}


swal({
  text: 'Are you sure?. Confirm to delete image.',
  buttons: ['Cancel', 'Ok'],
  dangerMode: true
})
  .then((value) => {
      if (value) {
          this.apiService.add_banner_images(updateimagedata).subscribe((data: any) => {
              if (data.statuscode = 204) {
                  swal({
                      text: 'Banner Imeges Deleted Successfully.',
                      buttons: [false],
                      dangerMode: true,
                      timer: 3000
                  });
                    location.reload();

              } else {
                  swal({
                      text: 'Something went wrong! Please try again.',
                      buttons: [false],
                      dangerMode: true,
                      timer: 2000
                  });
              }
          });
      }
  });
// this.apiService.add_banner_images(updateimagedata).subscribe(
//  (res:any)  => {
//     if (res.statuscode == 200) {
//       swal({
//         text: "Banner Image deleted Successfully.",
//         buttons: [false],
//         dangerMode: true,
//         timer: 3000
//       });
//       location.reload();
//       this.router.navigate(['/banner_images']);
//     } else {
//       swal({
//         text: "Something went wrong! Please try again.",
//         buttons: [false],
//         dangerMode: true,
//         timer: 2000
//       });
//     }
    
//     console.log("this.images_data",res)
//   },
//   err => console.error(err)
// );

  }
  urls = [];
  getFileDetails(e) {
    //console.log (e.target.files);  
    for (var i = 0; i < e.target.files.length; i++) {
      this.myFiles.push(e.target.files[i]);

      var reader = new FileReader();

      reader.onload = (event: any) => {
        console.log(event.target.result);
        this.urls.push(event.target.result);
      }

      reader.readAsDataURL(e.target.files[i]);
    }
  }

}
