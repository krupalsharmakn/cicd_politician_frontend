import { Component, OnInit } from '@angular/core';
 import { TranslateService } from '@ngx-translate/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private translate: TranslateService,private domSanitizer: DomSanitizer,  private matIconRegistry: MatIconRegistry,) {
         translate.setDefaultLang('en');
        this.matIconRegistry.addSvgIcon(
            'meeting',
            this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/business-meeting-svgrepo-com.svg')
        );
        this.matIconRegistry.addSvgIcon(
            'achievement',
            this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/achievement.svg')
        );
        this.matIconRegistry.addSvgIcon(
            'complaints',
            this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/complaints.svg')
        );
    }

    ngOnInit() {
    }
}
