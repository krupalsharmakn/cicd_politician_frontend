import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from 'src/app/shared/services/must-match.validation';
import { ApiService } from 'src/app/shared/services/api-service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import swal from 'sweetalert';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  type = "password";
  show = false;

  signupForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private _apiService: ApiService, private router: Router) { }
  
  toggleShow() {
    this.show = !this.show;
    if (this.show) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  
  trimValue(event) {
    event.target.value = event.target.value.trim();
  }
  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      user_name: ['', Validators.required],
      institute_name: ['', Validators.required],
    });
  }
  get f() { return this.signupForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.signupForm.valid) {
      this._apiService.signup(this.signupForm.value).subscribe((data: any) => {

          this.signupForm.reset();
          console.log("data.statuscode = " , data.statuscode)
          if (data.statuscode == 200) {
            this.router.navigate(['/login']);
            swal({
              text: "Your account has been created Succesfully",
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          } else if (data.statuscode == 203) {           
            swal({
              text: "User already exist.",
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          } else {
            swal({
              text: "Something went wrong! Please try again.",
              buttons: [false],
              dangerMode: true,
              timer: 3000
            });
          }
        },
        (err: HttpErrorResponse) => {

        });
    }
  }

}
